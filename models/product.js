'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var ProductSchema = Schema({
    name: String,
    brand: String,
    type: String,
    macros: {
        mass:Number,
        massUnit: String,
        calories:Number,
        hc:Number,
        suggars: Number,
        fats:Number,
        proteins:Number
    },
    macrosOriginal: {
        type: {
            mass:Number,
            massUnit: String,
            calories:Number,
            hc:Number,
            suggars: Number,
            fats:Number,
            proteins:Number
        },
        required: false
    }
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('product', ProductSchema, 'product');