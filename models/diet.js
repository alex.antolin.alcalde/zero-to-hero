'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
var Product = require('../models/product');

// Creamos el objeto del esquema y sus atributos
var DietSchema = Schema({
    title: String,
    description: String,
    date: String,
    meals: [{
        name: String,
        foods: [{}]
        
    }],
    userId: String
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('diet', DietSchema, 'diet');


