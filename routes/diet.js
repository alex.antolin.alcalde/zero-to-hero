'use strict'
// Cargamos el módulo de express para poder crear rutas
var express = require('express');
// Cargamos el controlador
var DietController = require('../controllers/diet');
// Llamamos al router
var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.use(md_auth.ensureAuth);

api.get('/diet', DietController.getDiet)
api.post('/diet', DietController.sendDiet)
// Exportamos la configuración
module.exports = api;