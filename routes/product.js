'use strict'
// Cargamos el módulo de express para poder crear rutas
var express = require('express');
// Cargamos el controlador
var ProductController = require('../controllers/product');
// Llamamos al router
var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.use(md_auth.ensureAuth);

api.get('/product', ProductController.getProduct);
api.get('/product/:id', ProductController.getProductById);
api.post('/product', ProductController.sendProduct)
api.post('/product/delete', ProductController.deleteProducts);
// Exportamos la configuración
module.exports = api;