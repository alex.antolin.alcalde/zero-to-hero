// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
/*   apiUrl: 'https://zero-to-hero-app.herokuapp.com/api', */
  apiUrl: 'http://localhost:3000/api',
  production: false,
  firebase: {
    apiKey: 'AIzaSyBLPYgvPE5-lOknve-8eOmCUjv_J89_gDQ',
    authDomain: 'zero2hero-140ab.firebaseapp.com',
    databaseURL: 'https://zero2hero-140ab.firebaseio.com',
    projectId: 'zero2hero-140ab',
    storageBucket: '',
    messagingSenderId: '318293475829',
    appId: '1:318293475829:web:32bd9e2af7265b853d60bc'
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
