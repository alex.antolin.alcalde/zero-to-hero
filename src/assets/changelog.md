All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),st
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2020-05-01
### Added
- Versioning and access in CHANGELOG.md.
- New login page.
- Creation group of products.

### Changed
- [DEV] - Facade pattern design (clean code & enhance performance).
- Fixing styling in diet and product page.


## [0.2.0] - 2020-02-18
### Changed
- [DEV] 🌟 Angular 8.2 to Angular 9.0.


## [0.1.0] - 2019-11-30
### Changed
- [DEV] Grouping modal component dialog as one.

### Fixed
- Update product list at adding or removing


## [0.0.1] - 2019-11-26
### Added
- New visual markdown changelog (ngx-showdown).

### Changed
- [DEV] Refactor all models about diet structure.

### Removed
- [DEV] Useless styles.
