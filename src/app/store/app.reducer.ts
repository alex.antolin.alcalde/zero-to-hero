import { ActionReducerMap } from '@ngrx/store';
import * as fromDiet from '@features/diet/store/diet.reducer';
import * as fromProductList from '@features/product/state/product-list.reducer';
import * as fromProject from '@shared/components/project/store/project.reducer';
import * as fromAuth from 'src/app/auth/state/auth.reducer';

export interface AppState {
    auth: fromAuth.State,
    diet: fromDiet.State,
    productList: fromProductList.State,
    project: fromProject.State
}

export const appReducer: ActionReducerMap<AppState> = {
    auth: fromAuth.authReducer,
    diet: fromDiet.dietReducer,
    productList: fromProductList.productsReducer,
    project: fromProject.projectReducer
}