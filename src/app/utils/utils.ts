import { FormGroup } from '@angular/forms';
import { DietModel } from '@core/models/diet.model';
import { GroupModel } from '@core/models/group.model';
import { MealModel } from '@core/models/meal.model';
import { PersonalDataModel } from '@core/models/user.model';
import { DietSummary } from '@features/diet/store/diet.reducer';
import { ProfileActivityEnum, ProfileActiviyValueEnum, ProfileGoalEnum, ProfileGoalValueEnum } from '@models/profile.model';
import { MacroData, Summary, SummaryMacro } from '@models/summary.model';
import { cloneDeep } from 'lodash';
import { CustomProductModel, ProductModel } from '../core/models/product.model';

export default class Utils {

    static MACROS = {
        hc: {
            energy: 4
        },
        fats: {
            energy: 9
        },
        proteins: {
            energy: 4
        }
    }

    static MOMENT_FORMAT = 'DD/MM/YYYY';

    static sanitizeDataBeforeSendToServer = (formGroup: FormGroup): any => {
        Object.keys(formGroup.controls).forEach(key => {
            const control = formGroup.get(key);
            if (typeof control.value === 'string' &&
                (control.value as string)?.trim() === '') {
                control.setValue(null);
            }
        });
    };

    static normalizeString = (input: string) => {
        return input.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    }

    static sortProductsByName(a, b) { return a.name.localeCompare(b.name, 0, { numeric: false }) }

    static ruleOfThree(macroToCalculate: number, product: ProductModel) {
        /*   return macroToCalculate * product.macros.mass / product.originalMacros.mass; */
    }


    // Personal energy calculation formulas

    // Calculate BMR
    /**
     * Men:  BMR = 88.362 + (13.397 x weight in kg) + (4.799 x height in cm) - (5.677 x age in years)
        Women: BMR = 447.593 + (9.247 x weight in kg) + (3.098 x height in cm) - (4.330 x age in years)
        */
    static BMR(gender: 'male' | 'female', weight, length, age): number {
        return (gender === 'male'
            ?
            88.362 + (13.397 * weight)
            + (4.799 * length)
            - (5.677 * age)
            :
            447.593 + (9.247 * weight)
            + (3.098 * length)
            - (4.330 * age)
        );
    }

    static TDEE(BMR: number, activityFactor: ProfileActivityEnum): number {
        return BMR * ProfileActiviyValueEnum[activityFactor];
    }

    static getGoalCalories(TDEE: number, goal: ProfileGoalEnum): number {
        return TDEE * ProfileGoalValueEnum[goal];
    }

    // Return total grams by macro & total calories
    static getGoalMacro(macroPercent: number, macro: string, calories: number): MacroData {
        const { energy } = this.MACROS[macro];

        return {
            value: (macroPercent / 100) * calories / energy,
            percent: (macroPercent / 100)
        }
    }

    static getCurrentMacroByMeals(meals: MealModel[]): SummaryMacro {
        let current: SummaryMacro = {
            calories: 0,
            fats: { value: 0, percent: 0 },
            hc: { value: 0, percent: 0 },
            proteins: { value: 0, percent: 0 }
        };

        meals.forEach((meal: MealModel) => {
            // Get all products (direct products + products in groups)
            /*  const allProducts = meal.getAllFoods();
             allProducts.forEach((product: ProductModel) => {
                 current.calories += product.macros.calories;
                 current.fats.value += product.macros.fats;
                 current.hc.value += product.macros.hc;
                 current.proteins.value += product.macros.proteins;
             }); */
        });

        /* const fatsPercent = (personal.fats * weight) * Utils.MACROS.fats.energy * 100 / goalCalories; */



        current.fats.percent = current.fats.value * this.MACROS['fats'].energy / current.calories;
        current.hc.percent = current.hc.value * this.MACROS['hc'].energy / current.calories;
        current.proteins.percent = current.proteins.value * this.MACROS['proteins'].energy / current.calories;

        return current;
    }

    static getCurrentMacroByProducts(products: ProductModel[]): SummaryMacro {
        let current: SummaryMacro = {
            calories: 0,
            fats: { value: 0, percent: 0 },
            hc: { value: 0, percent: 0 },
            proteins: { value: 0, percent: 0 }
        };


        products.forEach((product: ProductModel) => {
            current.calories += product.macros.calories;
            current.fats.value += product.macros.fats;
            current.hc.value += product.macros.hc;
            current.proteins.value += product.macros.proteins;
        });

        current.fats.percent = current.fats.value * this.MACROS['fats'].energy / current.calories;
        current.hc.percent = current.hc.value * this.MACROS['hc'].energy / current.calories;
        current.proteins.percent = current.proteins.value * this.MACROS['proteins'].energy / current.calories;

        return current;
    }


    static loadDietSummary(dietSummary: DietSummary, personalData: PersonalDataModel): DietSummary {
        const formatter = new Intl.NumberFormat('es-ES', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        });
        const dietSummaryClone = cloneDeep(dietSummary) as DietSummary;

        dietSummaryClone.kcal = {
            ...dietSummaryClone.kcal,
            goal: personalData.kcal
        }
        dietSummaryClone.hcs = {
            ...dietSummaryClone.hcs,
            goal: parseInt(formatter.format(personalData.kcal * (personalData.hc / 100))),
            totalGrms: parseInt(formatter.format(personalData.kcal * (personalData.hc / 100))) / this.MACROS.hc.energy
        }
        dietSummaryClone.fats = {
            ...dietSummaryClone.fats,
            goal: parseInt(formatter.format((personalData.fats * personalData.weight) * this.MACROS.fats.energy)),
            totalGrms: parseInt(formatter.format((personalData.fats * personalData.weight)))
        }
        dietSummaryClone.proteins = {
            ...dietSummaryClone.proteins,
            goal: parseInt(formatter.format((personalData.proteins * personalData.weight) * this.MACROS.proteins.energy)),
            totalGrms: parseInt(formatter.format((personalData.proteins * personalData.weight)))
        }
        return dietSummaryClone;
    }

    static fillDietSummaryWithDiet(meals: MealModel[], dietSummary: DietSummary): DietSummary {
        const formatter = new Intl.NumberFormat('es-ES', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        });

        const dietSummaryClone = cloneDeep(dietSummary) as DietSummary;
        dietSummaryClone.kcal.current = 0;
        dietSummaryClone.hcs.current = 0;
        dietSummaryClone.fats.current = 0;
        dietSummaryClone.proteins.current = 0;
        dietSummaryClone.kcal.currentGrms = 0;
        dietSummaryClone.hcs.currentGrms = 0;
        dietSummaryClone.fats.currentGrms = 0;
        dietSummaryClone.proteins.currentGrms = 0;
        meals.forEach(meal => {
            if (meal) {
                const summary: Summary = meal.getSummary();
                dietSummaryClone.kcal.current += summary.calories;
                dietSummaryClone.hcs.current += (summary.hcs * this.MACROS.hc.energy);
                dietSummaryClone.hcs.currentGrms += parseInt(formatter.format(summary.hcs));
                dietSummaryClone.fats.current += (summary.fats * this.MACROS.fats.energy);
                dietSummaryClone.fats.currentGrms += parseInt(formatter.format(summary.fats));
                dietSummaryClone.proteins.current += (summary.proteins * this.MACROS.proteins.energy);
                dietSummaryClone.proteins.currentGrms += parseInt(formatter.format(summary.proteins));
            }

        })


        return dietSummaryClone;
    }
}

type Item = ProductModel | GroupModel;

export const filterItemsByName = (items: Item[], input: string) => {
    return items.filter(item => {
        return Utils.normalizeString(item.name.toLocaleLowerCase()).includes(Utils.normalizeString(input.toLocaleLowerCase()))
    });
}

export const generateShoppingListByMacro = (days: MealModel[][]): { [key: string]: CustomProductModel[] } => {

    type foodType = 'fats' | 'hcs' | 'proteins' | 'fruits' | 'supp';

    const checkProductType = (product: ProductModel): foodType => {
        let type: foodType = 'hcs';
        if (product.macros.proteins > product.macros.fats && product.macros.proteins > product.macros.hc) {
            type = 'proteins'
        }
        if (product.macros.fats > product.macros.proteins && product.macros.fats > product.macros.hc) {
            type = 'fats'
        }
        if (product.type === 'fruta') {
            type = 'fruits'
        }

        if (product.type === 'supp') {
            type = 'supp'
        }
        return type;
    }

    let shoppingList: { [key: string]: CustomProductModel[] } = days.reduce((prev: {
        fats: CustomProductModel[],
        hcs: CustomProductModel[],
        proteins: CustomProductModel[],
        fruits: CustomProductModel[],
        supp: CustomProductModel[]
    }, current) => {
        const foods = current.map(meal => meal.products).flat(1);
        for (let food of foods) {
            const type: foodType = checkProductType(food.item);

            const match = (el: CustomProductModel) => el.item.brand === food.item.brand && el.item.name === food.item.name;
            const productFound = prev[type].findIndex(match);

            if (productFound !== -1) {
                prev[type][productFound] = {
                    item: prev[type][productFound].item,
                    mass: prev[type][productFound].mass + food.mass
                }
            }

            prev[type] = productFound !== -1 ? [...prev[type]] : [...prev[type], food]
        }
        return prev;
    }, {
        fats: [],
        hcs: [],
        proteins: [],
        fruits: [],
        supp: []
    });

    return shoppingList;

}