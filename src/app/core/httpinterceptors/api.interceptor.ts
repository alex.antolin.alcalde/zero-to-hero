import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const apiBaseUrl = '/api/';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if (this.shouldBeIntercepted(request)) {
                    this.showNotification((error ? this.getErrorMessage(error) : 'Error'), true);
                }
                return throwError(error);
            }));
    }

    private shouldBeIntercepted(request: HttpRequest<any>): boolean {
        return request.url.includes(apiBaseUrl);
    }

    private showNotification(message: any , isError: boolean) {
       /*  this.notifier.notify(isError ? 'error' : 'success', message ); */
    }

    private getErrorMessage(error: HttpErrorResponse): string {
        let message: string;
        if (!error || !error.message ) { return 'Uknown error'; }

        if (error.message) {
            message = error.message;
        } else {
            message = 'Error';
        }

        return message;
    }
}
