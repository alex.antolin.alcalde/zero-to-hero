import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";

import { PersonalDataModel, UserModel, WeightHistoricModel } from '@core/models/user.model';
import { environment } from '@env/environment';



@Injectable({
    providedIn: 'root'
})
export class ApiAuthService {
    private API_URL = `${environment.apiUrl}/users`;
    constructor(private http: HttpClient) {}

    saveUser(params: any): Observable<UserModel> {
        return this.http.post<UserModel>(this.API_URL, params).pipe(
            map(res => UserModel.deserializeObject(res, UserModel))
        );
    }

    savePersonalData(userID: string, personalData: PersonalDataModel): Observable<UserModel> {
        const url = `${this.API_URL}/${userID}`;
        return this.http.patch<UserModel>(`${url}/personalData`, personalData);
    }

    addWeight(userID: string, weights: WeightHistoricModel[]): Observable<UserModel> {
        const url = `${this.API_URL}/${userID}`;
        return this.http.patch<UserModel>(`${url}/weight`, weights);
    }

 /*    saveWeightHistoric(params: WeightHistoricModel): Observable<WeightHistoricModel> {
        return this.http.post<WeightHistoricModel>(`${this.API_URL}/weightHistoric`, params, httpOptions);
    }
     */

}
