import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Brands, Price, ProductModel } from '@core/models/product.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';


@Injectable({
    providedIn: 'root'
})
export class ApiProductService {
    private API_URL = `${environment.apiUrl}/products`;

    constructor(private http: HttpClient,
        private authService: AuthService) {}

    getProducts(): Observable<ProductModel[]> {
        return this.http.get<ProductModel[]>(this.API_URL, this.getHttpHeaders());
    }

    createProduct(formData: FormData): Observable<ProductModel> {
        return this.http.post<ProductModel>(this.API_URL, formData);
    }

    modifyProduct(id: string, formData: FormData): Observable<ProductModel> {
        return this.http.put<ProductModel>(`${this.API_URL}/${id}`, formData);
    }

    deleteProducts(ids: string[]): Observable<any> {
        let url = `${this.API_URL}?`;
        url = url.concat(ids.map((value) => `productID=${value}`).join('&'));

        return this.http.delete(url);
    }

   /*  addPrice(productID: string, prices: PriceModel[]): Observable<ProductModel> {
        const url = `${this.API_URL}/${productID}`;
        return this.http.patch<ProductModel>(url, prices, this.getHttpHeaders());
    } */
 
    searchOnMercadona(keyword: string): Observable<any[]> {
        const url = `${this.API_URL}/search/${keyword}`;
        return this.http.get<any[]>(url, this.getHttpHeaders());
    }

    getMacrosByProductID(id: string): Observable<any> {
        const url = `${this.API_URL}/macros/${id}`;
        return this.http.get<any>(url, this.getHttpHeaders());
    }

    getPriceByStoreId(store: Brands, storeId: string): Observable<Price> {
        const url = `${environment.apiUrl}/products/price/${store.toLocaleString().toLocaleLowerCase()}/${storeId}`;
        return this.http.get<Price>(url, this.getHttpHeaders());
    }

    getHttpHeaders() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.authService.getIdToken()}`
            })
        };
    }
    
}
