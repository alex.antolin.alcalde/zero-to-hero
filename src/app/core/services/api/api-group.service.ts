import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GroupModel } from '@core/models/group.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJaZXJvMkhlcm8iLCJuYW1lIjoiQWxleCJ9.KvvhkDw6XjpJPiORtuFMBzga6pakRC9I_Jd43653ZPA'
    })
};
@Injectable({
    providedIn: 'root'
})
export class ApiGroupService {
    private API_URL = `${environment.apiUrl}/groups`;

    constructor(private http: HttpClient) {}

    getGroups(): Observable<GroupModel[]> {
        return this.http.get<GroupModel[]>(this.API_URL, httpOptions);
    }

    createGroup(group: GroupModel): Observable<GroupModel> {
        return this.http.post<GroupModel>(this.API_URL, group, httpOptions);
    }

}
