import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";

import { environment } from '@env/environment';

import { DietModel } from '@core/models/diet.model';
import { AuthService } from "src/app/auth/auth.service";
import { BaseModel } from "@core/models/base.model";



@Injectable({
    providedIn: 'root'
})
export class ApiDietService {
    private API_URL = `${environment.apiUrl}/diets`;

    constructor(private http: HttpClient,
        private authService: AuthService) { }


    getDiets(): Observable<DietModel[]> {
        return this.http.get<DietModel[]>(this.API_URL, this.getHttpHeaders()).pipe(
            map((res) => {
                const diets = BaseModel.deserializeArray(res, DietModel);
                return diets;
            })
        );
    }

    createDiet(diet: DietModel): Observable<DietModel> {

        return this.http.post<DietModel>(this.API_URL, diet, this.getHttpHeaders());
    }

    modifyDiet(diet: any): Observable<DietModel> {
        return this.http.put<DietModel>(`${this.API_URL}/${diet._id}`, diet, this.getHttpHeaders());
    }

    deleteDiet(id: string): Observable<DietModel> {
        return this.http.delete<DietModel>(`${this.API_URL}/${id}`, this.getHttpHeaders());
    }


    getHttpHeaders() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.authService.getIdToken()}`
            })
        };
    }
}
