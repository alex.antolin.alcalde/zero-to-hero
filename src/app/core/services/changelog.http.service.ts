import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ChangelogService } from './changelog.service';


@Injectable({ providedIn: 'root' })
export class ChangelogHttpService {

    constructor(private http: HttpClient, private changelogService: ChangelogService) {
    }

    initializeApp(): Promise<void> {

        return new Promise(
            (resolve) => {
                this.http.get('assets/changelog.md', { responseType: 'text' })
                    .toPromise()
                    .then((response: any) => {
                        this.changelogService.changelog = response;
                        resolve();
                    })
            }
        );
    }
}