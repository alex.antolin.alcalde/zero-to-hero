import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ChangelogService {
    public changelog: any;

    constructor() {
        this.changelog = null;
    }
}