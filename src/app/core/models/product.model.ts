import { JsonObject, JsonProperty, Any } from 'json2typescript';
import { BaseModel } from './base.model';

export interface Price {
    isLoading?: boolean;
    unit_price: number;
    previous_price?: number;
    reference_format?: string;
    hasError?: boolean;
}

@JsonObject('ProductModel')
export class ProductModel extends BaseModel {

    @JsonProperty('_id', String, true)
    public id: string = undefined;

    @JsonProperty('storeId', String, true)
    public storeId: string = undefined;

    @JsonProperty('name', String)
    public name: string = undefined;

    @JsonProperty('image', String, true)
    public image: string = null;

    @JsonProperty('brand', String, true)
    public brand: Brands = null;

    @JsonProperty('type', String, true)
    public type: string | null = null;

    @JsonProperty('macros', Any, true)
    public macros: Macros = undefined;

    @JsonProperty('dateCreated', Date, true)
    public dateCreated: Date = undefined;

    public imageFile: File

    public price: Price;

    public toFormData = (): FormData => {
        const formData: FormData = new FormData();

        Object.keys(this).forEach(key => {
            if (this[key]!== null && this[key] !== undefined && typeof this[key] !== 'function') {
                const value = (key === 'macros' || Array.isArray(this[key]))  ? JSON.stringify(this[key]) : this[key];
                formData.append(key, value)
            }
        });
        return formData;
    }

}


@JsonObject('CustomProductModel')
export class CustomProductModel extends BaseModel {

    @JsonProperty('item', ProductModel)
    public item: ProductModel = undefined;

    @JsonProperty('mass', Number)
    public mass: number = 0;

    
    
}


export interface Macros {
    mass: number;
    massUnit: string;
    calories: number;
    hc: number;
    suggars: number;
    fats: number;
    proteins: number;
}

export enum Brands {
    PROZIS = 0,
    MERCADONA = 1,
    ECI = 2,
    HSN = 3,
    EROSKI = 4,
    DIA = 5,
    CARREFOUR = 6,
    PROE = 7,
    BONAREA = 8
}

