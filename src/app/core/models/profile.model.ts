interface WeightHistory {
    weight: number;
    date: Date;
}

export enum ProfileGoalEnum {
    FAT_LOSS = 'FAT_LOSS',
    MAINTAIN = 'MAINTAIN',
    GAIN_MUSCLE = 'GAIN_MUSCLE'
}

export enum ProfileGoalValueEnum {
    FAT_LOSS = 0.8,
    MAINTAIN = 1,
    GAIN_MUSCLE = 1.2
}

export enum ProfileActivityEnum {
    SEDENTARY = 'SEDENTARY',
    LIGHT = 'LIGHT',
    ACTIVE = 'ACTIVE',
    VERY_ACTIVE = 'VERY_ACTIVE',
    EXTREM_ACTIVE = 'EXTREM_ACTIVE'
}

export enum ProfileActiviyValueEnum {
    SEDENTARY = 1.3,
    LIGHT = 1.6,
    ACTIVE = 1.7,
    VERY_ACTIVE = 2.1,
    EXTREM_ACTIVE = 2.4
}

export class Profile {
    id: string;
    name: string;
    email: string;
    currentWeight: number;
    goalWeight: number;
    goal: ProfileGoalEnum;
    activityFactor: ProfileActivityEnum;
    weightHistory: WeightHistory[];

    constructor(
        id: string,
        name: string,
        email: string,
        currentWeight: number,
        goalWeight: number,
        goal: ProfileGoalEnum,
        activityFactor: ProfileActivityEnum) {
            this.id = id;
            this.name = name;
            this.email = email;
            this.currentWeight = currentWeight;
            this.goalWeight = goalWeight;
            this.goal = goal;
            this.activityFactor = activityFactor;
    }

    // Temporaty mandatory
    public isValid(): boolean {
        return !!Object.values(this).every(o => o === null);
    }
}
