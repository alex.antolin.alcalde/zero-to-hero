export interface MacroData {
    value: number,
    percent: number
}

export interface SummaryMacro {
    calories: number,
    fats: MacroData
    hc: MacroData,
    proteins: MacroData
}

export interface Summary {
    calories: number,
    fats: number
    hcs: number,
    proteins: number
    salt?: number;
    suggars?: number;
}
