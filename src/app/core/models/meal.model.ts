import { Any, JsonObject, JsonProperty } from 'json2typescript';
import { BaseModel } from './base.model';
import { GroupModel } from './group.model';
import { CustomProductModel, ProductModel } from './product.model';
import { Summary } from './summary.model';
import * as moment from 'moment';
import Utils from 'src/app/utils/utils';


@JsonObject('MealModel')
export class MealModel extends BaseModel {

    @JsonProperty('_id', Any)
    public id: string | undefined = undefined;

    @JsonProperty('name', String)
    public name: string = undefined;

    @JsonProperty('date', String, true)
    public date: any = moment(new Date()).format(Utils.MOMENT_FORMAT)

    @JsonProperty('products', [CustomProductModel], true)
    public products: CustomProductModel[] = [];

    @JsonProperty('groups', [GroupModel], true)
    public groups: GroupModel[] = [];

    /**
    * Merge foods and products into groups
    */
    public getAllFoods(): ProductModel[] {
        return this.products.reduce((prev, curr) => {
            return [...prev, curr]
        }, [])
    }

    public getSummary(): Summary {
        return this.products.reduce((prev, current) => {
            prev.calories += current.item.macros.calories * current.mass / current.item.macros.mass;
            prev.fats += current.item.macros.fats * current.mass / current.item.macros.mass;
            prev.hcs += current.item.macros.hc * current.mass / current.item.macros.mass;
            prev.proteins += current.item.macros.proteins * current.mass / current.item.macros.mass;
            return prev;
        }, {
            calories: 0,
            fats: 0,
            hcs: 0,
            proteins: 0,
        });
    }

}
