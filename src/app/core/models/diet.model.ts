import { Any, JsonObject, JsonProperty } from 'json2typescript';
import { BaseModel } from './base.model';
import { Goal } from './goal.model';
import { MealModel } from './meal.model';

@JsonObject('DietModel')
export class DietModel extends BaseModel {

    @JsonProperty('_id', String)
    public id: string = undefined;

    @JsonProperty('name', String)
    public name: string = '';

    @JsonProperty('description', String, true)
    public description: string = '';

    @JsonProperty('date', Any, true)
    public date: any = undefined;

    @JsonProperty('goals', [Any], true)
    public goals: Goal[] = undefined;

    @JsonProperty('days', [[MealModel]], true)
    public days: MealModel[][] = [];

    @JsonProperty('userId', String, true)
    public userId: string = undefined;

    @JsonProperty('active', Boolean, true)
    public active: boolean = undefined;

}
