
import { JsonObject, JsonProperty } from 'json2typescript';
import { BaseModel } from './base.model';
import { ProductModel } from './product.model';
import { Summary } from './summary.model';

@JsonObject('GroupModel')
export class GroupModel extends BaseModel {

    @JsonProperty('_id', String)
    public id: string = undefined;

    @JsonProperty('name', String, true)
    public name: string = undefined;

    @JsonProperty('foods', [ProductModel])
    public foods: ProductModel[] = [];

    @JsonProperty('userId', String, true)
    public userId: string = undefined;


     
    public getSummary(): Summary {
        let summary: Summary = {
            calories: 0,
            fats: 0,
            hcs: 0,
            proteins: 0,
        };

        const allProducts = this.foods;

        // Use reduce function
        allProducts.forEach((product: ProductModel) => {
            summary.calories += product.macros.calories;
            summary.fats += product.macros.fats;
            summary.hcs += product.macros.hc;
            summary.proteins += product.macros.proteins;
        });
        return summary;
    }
}
