import { JsonObject, JsonProperty, Any } from 'json2typescript';
import { BaseModel } from './base.model';
import { ProfileActivityEnum, ProfileGoalEnum } from './profile.model';
import { DateConverter } from './date.converter'

export interface MacroGoal {
    hc: number,
    fats: number,
    proteins: number
}


@JsonObject('PersonalDataModel')
export class PersonalDataModel extends BaseModel {
    @JsonProperty('activityFactor', String, true)
    public activityFactor: ProfileActivityEnum = undefined;

    @JsonProperty('age', Number)
    public age: number = undefined;

    @JsonProperty('gender', String)
    public gender: 'male' | 'female' = undefined;

    @JsonProperty('goal', String, true)
    public goal: ProfileGoalEnum = undefined;

    @JsonProperty('length', Number)
    public length: number = undefined;

    @JsonProperty('weight', Number)
    public weight: number = undefined;

    @JsonProperty('bmi', String, true)
    public bmi: number = null;

    @JsonProperty('bodyFat', String, true)
    public bodyFat: number = null;

    @JsonProperty('macroGoal', Any, true)
    public macroGoal: MacroGoal = null;

    @JsonProperty('kcal', Number, true)
    public kcal: number = null;

    @JsonProperty('hc', Number, true)
    public hc: number = null;

    @JsonProperty('proteins', Number, true)
    public proteins: number = null;

    @JsonProperty('fats', Number, true)
    public fats: number = null;
}

@JsonObject('WeightHistoricModel')
export class WeightHistoricModel extends BaseModel {

    @JsonProperty('date', DateConverter)
    public date: Date = null;

    @JsonProperty('weight', Number)
    public weight: number = undefined;

}

@JsonObject('UserModel')
export class UserModel extends BaseModel {

    @JsonProperty('_id', String, true)
    public id: string = undefined;

    @JsonProperty('googleId', String)
    public googleId: string = undefined;

    @JsonProperty('email', String)
    public email: string = undefined;

    @JsonProperty('image', String, true)
    public image: string = undefined;

    @JsonProperty('name', String)
    public name: string = undefined;

    @JsonProperty('personal', PersonalDataModel, true)
    public personal: PersonalDataModel = null

    @JsonProperty('progress', [WeightHistoricModel], true)
    public progress: Array<WeightHistoricModel> = null


}
