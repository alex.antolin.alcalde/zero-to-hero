import { JsonConvert, OperationMode, ValueCheckingMode } from 'json2typescript';
import { environment } from '@env/environment';

export abstract class BaseModel {

    public static jsonConvert: JsonConvert = new JsonConvert();

    constructor() {
    }

    public static serializeObject<T>(instance: T): any {
        return BaseModel.jsonConvert.serializeObject<any>(instance);
    }

    public static serializeArray<T>(instanceArray: Array<T>): Array<{}> {
        return BaseModel.jsonConvert.serializeArray<any>(instanceArray);
    }

    public static deserializeObject<T>(jsonObject: any, classReference: new () => T): T {
        return BaseModel.jsonConvert.deserializeObject<any>(jsonObject, classReference);
    }

    public static deserializeArray<T>(instanceArray: Array<any>, classReference: new () => T): Array<T> {
        return BaseModel.jsonConvert.deserializeArray<any>(instanceArray, classReference);
    }
}

/* BaseModel.jsonConvert.operationMode = environment.production ? OperationMode.ENABLE : OperationMode.LOGGING; */
BaseModel.jsonConvert.ignorePrimitiveChecks = false;
BaseModel.jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
