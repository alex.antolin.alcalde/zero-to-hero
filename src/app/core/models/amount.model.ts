export interface Amount {
    meals: AmountData[],
    total: AmountData
}

export interface AmountData {
    kcal?: number,
    hc?: number,
    fats?: number,
    proteins?: number
}