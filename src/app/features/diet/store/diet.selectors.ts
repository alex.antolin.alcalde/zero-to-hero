import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromDietReducer from './diet.reducer';

export const selectDietState = createFeatureSelector<fromDietReducer.State>('diet');

export const selectDayOfWeekState = createSelector(
  selectDietState,
  (state: fromDietReducer.State) => state.dayOfWeek
);

export const selectDietSummaryState = createSelector(
  selectDietState,
  (state: fromDietReducer.State) => state.summary
);

export const selectDietListState = createSelector(
  selectDietState,
  (state: fromDietReducer.State) => state.dietList
);

export const selectDietSelectedState = createSelector(
  selectDietState,
  (state: fromDietReducer.State) => state.dietSelected
);

export const selectShoppingListState = createSelector(
  selectDietState,
  (state: fromDietReducer.State) => state.shoppingList
);
