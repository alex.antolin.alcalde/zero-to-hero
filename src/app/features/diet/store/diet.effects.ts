import { Injectable } from '@angular/core';
import { DietModel } from '@core/models/diet.model';
import { ApiDietService } from '@core/services/api/api-diet.service';
import * as dietsActions from '@features/diet/store/diet.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { EMPTY, Subject, from, of, throwError } from 'rxjs';
import { catchError, concatMap, exhaustMap, map, mergeMap, takeUntil, withLatestFrom } from 'rxjs/operators';
import { selectUserState } from 'src/app/auth/state/auth.selectors';
import * as fromApp from 'src/app/store/app.reducer';
import Utils from 'src/app/utils/utils';
import { DietSummary } from './diet.reducer';
import { selectDietSelectedState, selectDietSummaryState } from './diet.selectors';
import { BaseModel } from '@core/models/base.model';
import { CustomProductModel, Price } from '@core/models/product.model';
import { SHOPPING_LIST_GROUP_TYPE } from '@shared/components/shopping-list/shopping-list.component';
import { cloneDeep } from 'lodash';
import { ApiProductService } from '@core/services/api/api-product.service';


@Injectable()
export class DietEffects {

    private stopCalculatePrices = new Subject<void>();
    private stop = false;

    loadDiets$ = createEffect(() => this.actions$.pipe(
        ofType(dietsActions.loadDiets),
        exhaustMap((action) => {
            return this.apiDiet.getDiets().pipe(
                mergeMap(diets => {
                    const actions: Action[] = [];
                    actions.push(dietsActions.fillDiets({ diets: diets?.length > 0 ? diets : [] }))

                    if (diets?.length > 0) {
                        actions.push(dietsActions.selectDiet({ diet: diets[0] }));
                        actions.push(dietsActions.generateShoppingList({
                            groupType: SHOPPING_LIST_GROUP_TYPE.MACRO
                        }))
                    }
                    return actions;
                }),
                catchError(() => EMPTY)
            )

        })
    ));

    createDiet$ = createEffect(() => this.actions$.pipe(
        ofType(dietsActions.createDiet),
        exhaustMap((action) => {

            return this.apiDiet.createDiet(action.diet).pipe(
                mergeMap(result => {
                    const diet = DietModel.deserializeObject(result, DietModel);
                    return [
                        dietsActions.createDietSuccess({ diet: diet }),
                        dietsActions.selectDiet({ diet: diet })
                    ]
                }),
                catchError(() => EMPTY)
            )

        })
    ));

    /*  modifyDiet$ = createEffect(() => this.actions$.pipe(
         ofType(dietsActions.modifyDiet),
         withLatestFrom(this.store$.select('diet')),
         exhaustMap(([action, dietState]) => {
 
             return this.apiDiet.modifyDiet(dietState.dietSelected.id, dietState.dietSelected).pipe(
                 map(result => {
                     const diet = DietModel.deserializeObject(result, DietModel);
                     return dietsActions.modifyDietSuccess({ dietModified: diet })
                 }),
                 catchError(() => EMPTY)
             )
 
         })
     )); */

    deleteDiet$ = createEffect(() => this.actions$.pipe(
        ofType(dietsActions.deleteDiet),
        withLatestFrom(this.store$.select('diet')),
        exhaustMap(([action, dietState]) => {
            return this.apiDiet.deleteDiet(dietState.dietSelected.id).pipe(
                map(() => {
                    return dietsActions.loadDiets();
                }),
                catchError(() => EMPTY)
            )
        })
    ));

    calculateDietSummary$ = createEffect(() => this.actions$.pipe(
        ofType(dietsActions.calculateDietSummary),
        withLatestFrom(this.store$.select(selectUserState), this.store$.select(selectDietSummaryState)),
        map(([action, user, dietSummary]) => {
            const summary: DietSummary = Utils.loadDietSummary(dietSummary, user.personal);
            return dietsActions.loadDietSummary({ summary: summary });
        })
    ));

    saveDiet$ = createEffect(() => this.actions$.pipe(
        ofType(
            dietsActions.deleteMeal,
            dietsActions.cloneDayOfDiet,
            dietsActions.addMealToDiet,
            dietsActions.addProducToMeal,
            dietsActions.deleteProductOfMeal,
            dietsActions.editMassOfProduct),
        withLatestFrom(
            this.store$.select(selectUserState),
            this.store$.select(selectDietSelectedState)),
        exhaustMap(([action, user, dietSelected]) => {
            const diet = cloneDeep(BaseModel.serializeObject(dietSelected));
            diet.days.forEach(day => {
                day = day.map(m => {
                    m.products = m.products.map(p => ({
                        item: p.item._id,
                        mass: p.mass
                    } as CustomProductModel));
                    return m;
                })
            });
            return this.apiDiet.modifyDiet(diet).pipe(
                map(result => {
                    const diet = DietModel.deserializeObject(result, DietModel);
                    /*  return dietsActions.modifyDietSuccess({ dietModified: diet }) */
                }),
                catchError(() => EMPTY)
            )
        })
    ), { dispatch: false });


    calculatePrices$ = createEffect(() => this.actions$.pipe(
        ofType(dietsActions.calculatePrices),
        exhaustMap((action) => {
            this.stop = false;
            let { productReferenceList } = action;
            return from(productReferenceList).pipe(
                takeUntil(this.stopCalculatePrices),
                concatMap((pR, index) => {
                    if (this.stop) {
                        return throwError(() => new Error('Calculate prices stopped manually.'));
                    }
                    return this.apiProduct.getPriceByStoreId(pR.p.item.brand, pR.p.item.storeId).pipe(
                        catchError(() => {
                            this.store$.dispatch(dietsActions.calculatePriceError({
                                pR
                            }));
                            return EMPTY;
                            
                        }),
                        map((price: Price) => ({
                            price, pR
                        }))
                    );
                }),
                map(pRPrice => {
                    return dietsActions.calculatePriceSuccess({
                        price: pRPrice.price,
                        pR: pRPrice.pR
                    });

                })
            );
        }
    )));


    constructor(
        private store$: Store<fromApp.AppState>,
        private actions$: Actions,
        private apiDiet: ApiDietService,
        private apiProduct: ApiProductService) { }
}
