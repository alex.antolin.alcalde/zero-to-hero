import { DietModel } from '@core/models/diet.model';
import { MealModel } from '@core/models/meal.model';
import { CustomProductModel, Price, ProductModel } from '@core/models/product.model';
import { createAction, props } from '@ngrx/store';
import { DietSummary } from './diet.reducer';
import { SHOPPING_LIST_GROUP_TYPE, productReferenceShoppingList } from '@shared/components/shopping-list/shopping-list.component';

export const LOAD_DIETS = '[Diet] Load diets';
export const FILL_DIETS = '[Diet] Fill diets';
export const SELECT_DIET = '[Diet] Select diet';
export const CREATE_DIET = '[Diet] Create diet';
export const CREATE_DIET_SUCCESS = '[Diet] Create diet success';
export const MODIFY_DIET = '[Diet] Modify diet';
export const MODIFY_DIET_SUCCESS = '[Diet] Modify diet success';
export const DELETE_DIET = '[Diet] Delete diet'; 
export const ADD_PRODUCT_TO_MEAL = '[Diet] Add product to meal';
export const SAVE_MEAL = '[Diet] Save meal';
export const CALCULATE_DIET_SUMMARY = '[Diet] Calculate diet summary';
export const LOAD_DIET_SUMMARY = '[Diet] Load diet summary';
export const ADD_MEAL_TO_DIET = '[Diet] Add meal to diet';
export const MOVE_MEAL_IN_DIET = '[Diet] Move meal in diet';
export const ACTIVE_MEAL_OPTION = '[Diet] Active meal option';
export const DELETE_PRODUCT_OF_MEAL = '[Diet] Delete product of meal';
export const CHANGE_DAY_OF_WEEK = '[Diet] Change day of week';
export const CLONE_DAY_OF_DIET = '[Diet] Clone day of diet';
export const EDIT_MASS_OF_PRODUCT = '[Diet] Edit mass of product';
export const DELETE_MEAL = '[Diet] Delete meal';
export const GENERATE_SHOPPING_LIST = '[Diet] Generate shopping list';
export const CALCULATE_PRICES = '[Diet] Calculate prices';
export const CALCULATE_PRICE_SUCCESS = '[Diet] Calculate price success';
export const CALCULATE_PRICE_ERROR = '[Diet] Calculate price error';

export const changeDayOfWeek = createAction(
    CHANGE_DAY_OF_WEEK,
    props<{ day: number }>()
)

export const generateShoppingList = createAction(
    GENERATE_SHOPPING_LIST,
    props<{ groupType: SHOPPING_LIST_GROUP_TYPE }>()
)

export const calculatePrices = createAction(
    CALCULATE_PRICES,
    props<{ productReferenceList: productReferenceShoppingList[] }>()
)

export const calculatePriceSuccess = createAction(
    CALCULATE_PRICE_SUCCESS,
    props<{ price: Price, pR: productReferenceShoppingList }>()
)

export const calculatePriceError = createAction(
    CALCULATE_PRICE_ERROR,
    props<{ pR: productReferenceShoppingList }>()
)

export const cloneDayOfDiet = createAction(
    CLONE_DAY_OF_DIET,
    props<{ dayToBeClone: number, daysToBeOverride: boolean[] }>()
)

export const deleteMeal = createAction(
    DELETE_MEAL,
    props<{ indexMeal: number }>()
)

export const loadDiets = createAction(
    LOAD_DIETS
)

export const fillDiets = createAction(
    FILL_DIETS,
    props<{ diets: DietModel[] }>()
)

export const selectDiet = createAction(
    SELECT_DIET,
    props<{ diet: DietModel }>()
)

export const createDiet = createAction(
    CREATE_DIET,
    props<{ diet: DietModel }>()
)

export const createDietSuccess = createAction(
    CREATE_DIET_SUCCESS,
    props<{ diet: DietModel }>()
)

export const modifyDiet = createAction(
    MODIFY_DIET,
)

export const modifyDietSuccess = createAction(
    MODIFY_DIET_SUCCESS,
    props<{ dietModified: DietModel }>()
)

export const deleteDiet = createAction(
    DELETE_DIET
)

export const editMassOfProduct = createAction(
    EDIT_MASS_OF_PRODUCT,
    props<{ product: CustomProductModel, mealIndex: number }>()
)

export const addProducToMeal = createAction(
    ADD_PRODUCT_TO_MEAL,
    props<{ product: ProductModel, mealIndex: number }>()
)

export const saveMeal = createAction(
    SAVE_MEAL,
    props<{ diet: DietModel }>()
)

export const calculateDietSummary = createAction(
    CALCULATE_DIET_SUMMARY
)

export const loadDietSummary = createAction(
    LOAD_DIET_SUMMARY,
    props<{ summary: DietSummary }>()
)

export const addMealToDiet = createAction(
    ADD_MEAL_TO_DIET,
    props<{ meal: MealModel }>()
)

export const moveMealInDiet = createAction(
    MOVE_MEAL_IN_DIET,
    props<{ previous: number, current: number }>()
)

export const activeMealOption = createAction(
    ACTIVE_MEAL_OPTION,
    props<{ indexMeal: number, indexMealOption: number }>()
)

export const deleteProductOfMeal = createAction(
    DELETE_PRODUCT_OF_MEAL,
    props<{ product: ProductModel, mealIndex: number}>()
)