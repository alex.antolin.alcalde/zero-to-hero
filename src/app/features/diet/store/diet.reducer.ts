import { DietModel } from "@core/models/diet.model";
import { createReducer, on } from "@ngrx/store";
import { cloneDeep } from 'lodash';
import Utils, { generateShoppingListByMacro } from "src/app/utils/utils";
import * as dietsActions from './diet.actions';
import { CustomProductModel, ProductModel } from "@core/models/product.model";
import { SHOPPING_LIST_GROUP_TYPE } from "@shared/components/shopping-list/shopping-list.component";

export interface DietSummary {
    kcal: DietSummaryItem,
    hcs: DietSummaryItem,
    fats: DietSummaryItem,
    proteins: DietSummaryItem
}

export interface DietSummaryItem {
    current: number;
    currentGrms?: number;
    goal: number;
    totalGrms?: number;
}

export interface State {
    dayOfWeek: number;
    dietList: DietModel[];
    dietSelected: DietModel,
    editMeal: number,
    shoppingList: { [key: string]: CustomProductModel[] },
    summary: DietSummary
}

const initialState: State = {
    dayOfWeek: new Date().getDay(),
    dietList: [],
    dietSelected: new DietModel(),
    editMeal: null,
    shoppingList: {},
    summary: {
        kcal: {
            current: 0,
            goal: 0
        },
        hcs: {
            current: 0,
            goal: 0
        },
        fats: {
            current: 0,
            goal: 0
        },
        proteins: {
            current: 0,
            goal: 0
        }
    }
}

const _dietReducer = createReducer(initialState,
    on(dietsActions.cloneDayOfDiet, (state, { dayToBeClone, daysToBeOverride }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        const dayMaster = dietClone.days[dayToBeClone];
        for (let i = 0; i < daysToBeOverride.length; i++) {
            if (daysToBeOverride[i]) {
                dietClone.days[i] = dayMaster
            }
        }

        return {
            ...state,
            dietSelected: dietClone
        }

    }),
    on(dietsActions.changeDayOfWeek, (state, { day }) => {
        const newSummary = Utils.fillDietSummaryWithDiet(state.dietSelected.days[day], state.summary);
        return {
            ...state,
            dayOfWeek: day,
            summary: newSummary
        }
    }),
    on(dietsActions.deleteMeal, (state, { indexMeal }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        const dayMeal = dietClone.days[state.dayOfWeek];
        dietClone.days[state.dayOfWeek] = dietClone.days[state.dayOfWeek].filter((meal, i) => i !== indexMeal)
        return {
            ...state,
            dietSelected: dietClone
        }
    }),
    on(dietsActions.fillDiets, (state, { diets }) => ({
        ...state,
        dietList: [...diets],
        dietSelected: null
    })),
    on(dietsActions.selectDiet, (state, { diet }) => {
        const newSummary = Utils.fillDietSummaryWithDiet(diet.days[state.dayOfWeek], state.summary);
        return ({
            ...state,
            dietSelected: diet,
            summary: newSummary
        })
    }),
    on(dietsActions.createDietSuccess, (state, { diet }) => ({
        ...state,
        dietList: [...state.dietList, diet],
        dietSelected: diet,
    })),
    on(dietsActions.modifyDietSuccess, (state, { dietModified }) => {
        const dietListUpdated = [...state.dietList.filter(d => d.id !== dietModified.id), dietModified];

        return ({
            ...state,
            dietList: [...dietListUpdated],
            dietSelected: dietModified
        })
    }),
    on(dietsActions.saveMeal, (state, { diet }) => {
        const newSummary = Utils.fillDietSummaryWithDiet(diet.days[state.dayOfWeek], state.summary);
        return {
            ...state,
            dietSelected: diet,
            summary: newSummary
        }
    }),
    on(dietsActions.loadDietSummary, (state, { summary }) => ({
        ...state,
        summary: summary
    })),
    on(dietsActions.addMealToDiet, (state, { meal }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        dietClone.days[state.dayOfWeek].push(meal);

        return ({
            ...state,
            dietSelected: dietClone
        })
    }),
    on(dietsActions.moveMealInDiet, (state, { previous, current }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        var mealToMove = dietClone.days[state.dayOfWeek][previous];
        dietClone.days[state.dayOfWeek].splice(previous, 1);
        dietClone.days[state.dayOfWeek].splice(current, 0, mealToMove);


        return ({
            ...state,
            dietSelected: dietClone
        })
    }),
    on(dietsActions.generateShoppingList, (state, { groupType }) => {
        const days = state.dietSelected?.days || [];

        let shoppingList = state.shoppingList;
        if (groupType === SHOPPING_LIST_GROUP_TYPE.MACRO) {
            shoppingList = generateShoppingListByMacro(days);
        }

        return {
            ...state,
            shoppingList: { ...shoppingList }
        }
    }),
    on(dietsActions.calculatePrices, (state, { productReferenceList }) => {
        let shoppingList = cloneDeep(state.shoppingList) as { [key: string]: CustomProductModel[] };
        productReferenceList.forEach(pr => {
            const customProduct = cloneDeep(pr.p) as CustomProductModel;
            customProduct.item.price = {
                ...customProduct.item.price,
                isLoading: true
            }
            shoppingList[pr.g][pr.i] = customProduct;
        })

        return {
            ...state,
            shoppingList: { ...shoppingList }
        }
    }),
    on(dietsActions.calculatePriceSuccess, (state, { price, pR }) => {
        let shoppingList = cloneDeep(state.shoppingList) as { [key: string]: CustomProductModel[] };
        const productUpdated = cloneDeep(shoppingList[pR.g][pR.i]) as CustomProductModel;
        productUpdated.item.price = {
            ...price,
            isLoading: false
        }
        shoppingList[pR.g][pR.i] = productUpdated

        return {
            ...state,
            shoppingList: { ...shoppingList }
        }
    }),
    on(dietsActions.calculatePriceError, (state, { pR }) => {
        let shoppingList = cloneDeep(state.shoppingList) as { [key: string]: CustomProductModel[] };
        const productUpdated = cloneDeep(shoppingList[pR.g][pR.i]) as CustomProductModel;
        productUpdated.item.price = {
            ...productUpdated.item.price,
            isLoading: false,
            hasError: true
        }
        shoppingList[pR.g][pR.i] = productUpdated

        return {
            ...state,
            shoppingList: { ...shoppingList }
        }
    }),
    on(dietsActions.editMassOfProduct, (state, { product, mealIndex }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        const currentProducts = dietClone.days[state.dayOfWeek][mealIndex].products

        const productIndex = currentProducts.findIndex(c => c.item.id === product.item.id);
        dietClone.days[state.dayOfWeek][mealIndex].products[productIndex] = product;
        
        const newSummary = Utils.fillDietSummaryWithDiet(dietClone.days[state.dayOfWeek], state.summary);

        return {
            ...state,
            dietSelected: dietClone,
            summary: newSummary
        }
    }),
    on(dietsActions.addProducToMeal, (state, { product, mealIndex }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        const currentProducts: CustomProductModel[] = dietClone.days[state.dayOfWeek][mealIndex].products;

        const productFound = currentProducts.find(c => c.item.id === product.id);
        if (Boolean(productFound)) {
            productFound.mass = product.macros.mass;
        } else {
            dietClone.days[state.dayOfWeek][mealIndex].products = [...currentProducts, {
                item: product,
                mass: product.macros.mass
            }];
        }

        return {
            ...state,
            dietSelected: dietClone
        }
    }),
    on(dietsActions.deleteProductOfMeal, (state, { product, mealIndex }) => {
        const dietClone = cloneDeep(state.dietSelected) as DietModel;
        let dayUpdated = [...dietClone.days[state.dayOfWeek]];
        dayUpdated[mealIndex].products = dayUpdated[mealIndex].products.filter(
            f => f.item.id !== product.id
        );
        dietClone.days[state.dayOfWeek] = [...dayUpdated];
        return {
            ...state,
            dietSelected: dietClone
        }
    }),
)

export function dietReducer(state, action) {
    return _dietReducer(state, action)
}
