import { Pipe, PipeTransform } from '@angular/core';
import { MealModel } from '@core/models/meal.model';

@Pipe({
    name: 'mealTextSummary'
})
export class MealTextSummaryPipe implements PipeTransform {

    transform(mealOption: MealModel): string {
        let message;
        const foods = mealOption.getAllFoods().map(product => {
            return `(${product.macros.mass} ${product.macros.massUnit}) ${product.name}`;
        });

        message = foods.slice(0, -1).join(', ').concat(' y ' + foods.slice(-1));

        return message || 'Meal option is empty!';
    }
}