import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MealModel } from '@core/models/meal.model';
import { ConfirmDeleteMealComponent } from '@shared/components/confirm-delete-meal/confirm-delete-meal.component';

@Component({
    selector: 'z2h-meals',
    templateUrl: './meals.component.html',
    styleUrls: ['./meals.component.scss']
})
export class MealsComponent {

    @Input() meals: MealModel[] = [];

    constructor(private dialog: MatDialog){}
  
    deleteMeal(indexMeal: number) {
        this.dialog.open(ConfirmDeleteMealComponent, {
            width: '405px',
            height: '220px',
            disableClose: true,
            backdropClass: 'action-dialog-backdrop',
            autoFocus: false,
            data: {
                indexMeal: indexMeal
            }
        });
    }
}
