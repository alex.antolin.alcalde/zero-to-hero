import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MealModel } from '@core/models/meal.model';
import { Summary } from '@core/models/summary.model';
import { Subject } from 'rxjs';

@Component({
    selector: 'z2h-meal-summary',
    templateUrl: './meal-summary.component.html',
    styleUrls: ['./meal-summary.component.scss']
})
export class MealSummaryComponent implements OnInit, OnDestroy {

    onDestroy$ = new Subject<void>();
    @Input() meal: MealModel;
    summary: Summary = null;

    ngOnInit(): void {
        this.summary = this.meal.getSummary();
    }


    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

}
