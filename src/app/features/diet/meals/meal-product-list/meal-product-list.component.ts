import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MealModel } from '@core/models/meal.model';
import { CustomProductModel, ProductModel } from '@core/models/product.model';
import { DietService } from '@features/diet/diet.service';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';
import * as dietsActions from '../../store/diet.actions';


@Component({
    selector: 'z2h-meal-product-list',
    templateUrl: './meal-product-list.component.html',
    styleUrls: ['./meal-product-list.component.scss']
})
export class MealProductListComponent implements OnInit, OnDestroy {

    onDestroy$ = new Subject<void>();
    @Input() meal: MealModel;
    @Input() mealIndex: number;
    products: ProductModel[] = [];

    constructor(
        private dietService: DietService,
        private store: Store<fromApp.AppState>) {
    }

    ngOnInit(): void {
        this.products = this.meal.getAllFoods();
    }


    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    drop(event: CdkDragDrop<ProductModel[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            const productToAddMeal: ProductModel = event.previousContainer.data[event.previousIndex];
            this.store.dispatch(dietsActions.addProducToMeal({
                product: productToAddMeal, mealIndex: this.mealIndex
            }))
        }
    }

    onEditProduct(customProduct: CustomProductModel): void {
        this.dietService.editMassOfProduct(customProduct, this.mealIndex);
    }

    onDeleteProduct(product: ProductModel): void {
        this.store.dispatch(dietsActions.deleteProductOfMeal({
            product,
            mealIndex: this.mealIndex
        }))
    }


}
