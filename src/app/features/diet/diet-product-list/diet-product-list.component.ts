import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProductModel } from '@core/models/product.model';
import * as productsActions from '@features/product/state/product-list.actions';
import { selectProductListState } from '@features/product/state/products.selectors';
import { Store } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, EMPTY, finalize, Observable, startWith, Subject, switchMap, takeUntil, tap } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';


@Component({
    selector: 'z2h-diet-product-list',
    templateUrl: './diet-product-list.component.html',
    styleUrls: ['./diet-product-list.component.scss']
})
export class DietProductListComponent implements OnInit, OnDestroy {

    cdkMealListNames: string[] = [];

    _numMeals: number;
    get numMeals(): number {
        return this._numMeals;
    }
    @Input() set numMeals(numMeals: number) {
        this._numMeals = numMeals;
        for (let i = 0; i < numMeals; i++) {
            this.cdkMealListNames = [...this.cdkMealListNames, `mealProductsCdk-${i}`];
        }
       
    }

    onDestroy$ = new Subject<void>();
    products$: Observable<ProductModel[]> = this.store.select(selectProductListState);
    /*     groups$: Observable<GroupModel[]> = this.store.select(selectProductListState); */
    searchControl: FormControl = new FormControl('');
    searchLoading: boolean = false;
    searchType: 'product' | 'meal' = 'product'


    constructor(
        private store: Store<fromApp.AppState>) {
    }

    ngOnInit() {
        this.store.dispatch(productsActions.loadProducts());

        this.searchControl.valueChanges.pipe(
            startWith(''),
            debounceTime(400),
            distinctUntilChanged(),
            takeUntil(this.onDestroy$),
            tap(() => {
                this.searchLoading = true;
            }),
            switchMap((value: any) => {
                this.store.dispatch(productsActions.searchProductByName({ input: value }));
                return EMPTY;
            })).subscribe()
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

}
