import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ProductCardModule } from '@shared/components/product-card/product-card.module';
import { ShoppingListModule } from '@shared/components/shopping-list/shopping-list.module';
import { MaterialModule } from '@shared/material.module';
import { SharedModule } from '@shared/shared.module';
import { DietProductListComponent } from './diet-product-list/diet-product-list.component';
import { DietSummaryComponent } from './diet-summary/diet-summary.component';
import { DietSummaryPipe } from './diet-summary/diet-summary.pipe';
import { DietComponent } from './diet.component';
import { MealTextSummaryPipe } from './meal-text-summary.pipe';
import { MealProductListComponent } from './meals/meal-product-list/meal-product-list.component';
import { MealSummaryComponent } from './meals/meal-summary/meal-summary.component';
import { MealsComponent } from './meals/meals.component';

const routes: Routes = [
    {
        path: '',
        component: DietComponent
    }
];

@NgModule({
    declarations: [
        DietComponent,
        DietSummaryComponent,
        MealsComponent,
        MealSummaryComponent,
        MealProductListComponent,
        DietProductListComponent,
        DietSummaryPipe,
        MealTextSummaryPipe
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        ProductCardModule,
        SharedModule,
        MaterialModule,
        ShoppingListModule,
        RouterModule.forChild(routes)]
})
export class DietModule {}
