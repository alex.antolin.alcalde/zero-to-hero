import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DietModel } from '@core/models/diet.model';
import { MealModel } from '@core/models/meal.model';
import { CustomProductModel, ProductModel } from '@core/models/product.model';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import * as fromApp from 'src/app/store/app.reducer';
import * as dietsActions from './store/diet.actions';
import cloneDeep from 'lodash';
import { BaseModel } from '@core/models/base.model';

@Injectable({
    providedIn: 'root'
})
export class DietService {

    constructor(
        private dialog: MatDialog,
        private store: Store<fromApp.AppState>) { }


    selectDiet(diet: DietModel) {
        this.store.dispatch(dietsActions.selectDiet({ diet: diet }));
    }

    createDiet(name: string, description: string) {
        const newDiet: DietModel = new DietModel();
        newDiet.name = name;
        newDiet.description = description;
        newDiet.days = this.generateSampleDayMeals();
        newDiet.date = moment().toDate();

        this.store.dispatch(dietsActions.createDiet({ diet: newDiet }));
    }

    deleteDiet() {
        this.store.dispatch(dietsActions.deleteDiet())
    }

    addMealToDiet(name: string) {
        const meal = new MealModel();
        meal.name = name;
        

        this.store.dispatch(dietsActions.addMealToDiet({ meal: meal }));
    }

    editMassOfProduct(customProduct: CustomProductModel, mealIndex: number) {
        this.store.dispatch(dietsActions.editMassOfProduct({
            product: customProduct, mealIndex: mealIndex
        }))
    }

    /**
    * Generate sample meals with any foods
    */
    private generateSampleDayMeals(): MealModel[][] {
        const sampleMeals = ['Desayuno', 'Almuerzo', 'Comida', 'Cena'];

        let days = [];
        for (let i = 0; i < 7; i++) {
            days.push(sampleMeals.map((sampleMeal: string) => {
                const meal: MealModel = new MealModel();
                meal.name = sampleMeal;
                meal.products = [];
                meal.groups = [];
                return meal;
            }));
        }

        return days;


    }

}