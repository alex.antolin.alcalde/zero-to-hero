import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Summary } from '@core/models/summary.model';
import * as productsActions from '@features/product/state/product-list.actions';
import { DietModel } from '@models/diet.model';
import { Store } from '@ngrx/store';
import { ConfirmDeleteMealComponent } from '@shared/components/confirm-delete-meal/confirm-delete-meal.component';
import { EditDietDialogComponent } from '@shared/components/edit-diet/edit-diet.component';
import { ReportDietComponent } from '@shared/components/report-diet/report-diet.component';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as fromApp from 'src/app/store/app.reducer';
import * as dietsActions from './store/diet.actions';
import { selectDayOfWeekState, selectDietSelectedState } from './store/diet.selectors';

@Component({
    selector: 'app-diet',
    templateUrl: './diet.component.html',
    styleUrls: ['./diet.component.scss']
})
export class DietComponent implements OnInit, OnDestroy {
    @HostListener('window:scroll', ['$event'])
    onScroll(event) {
        const verticalOffset = window.pageYOffset 
        || document.documentElement.scrollTop 
        || document.body.scrollTop || 0;
    
        this.sticky = verticalOffset > 257

    }

    sticky = false;

    onDestroy$ = new Subject<void>();
    isDragging = false;

    dayOfWeek$: Observable<number> = this.store.select(selectDayOfWeekState).pipe(takeUntil(this.onDestroy$));
    diet$: Observable<DietModel> = this.store.select(selectDietSelectedState).pipe(takeUntil(this.onDestroy$));
    summary$: Observable<Summary>;
    summaries$: Observable<Summary[]>;

    selected: Date | null;
    /*   dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {
  
          const classArr = [];
          // Only highligh dates inside the month view.
          if (view === 'month') {
              if (new Date().setHours(0, 0, 0, 0) === cellDate.getTime()) {
                  classArr.push('current-day');
              }
  
              const date = cellDate.getDate();
              if ( date === 1 || date === 20) {
                  classArr.push('example-custom-date-class');
              }
  
          }
          classArr.join(' ');
  
          return classArr;
      };
   */
    constructor(
        private store: Store<fromApp.AppState>,
        public dialog: MatDialog) {
    }

    ngOnInit() {
        this.store.dispatch(productsActions.setDisplayedColumns({
            displayedColumns: ['name', 'brand', 'mass', 'calories', 'hc', 'fats', 'proteins']
        }))
        this.store.dispatch(dietsActions.calculateDietSummary());
        this.store.dispatch(dietsActions.loadDiets());
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    drop(diet: DietModel, event: CdkDragDrop<string[]>) {
        this.store.dispatch(dietsActions.moveMealInDiet({ previous: event.previousIndex, current: event.currentIndex }));
    }

    droped(event: any) {
    }

    drag(isDragging: boolean) {
        this.isDragging = isDragging;
    }




    addMealOption(indexMeal: number) {
        /*  this.store.dispatch(dietsActions.addMealOption({ indexMeal: indexMeal })); */
    }


    /* Add foods to a diet's meal */
    editMealOption(indexMeal: number, mealOption: any, indexMealOption: number) {
        /*   this.store.dispatch(projectActions.selectMealOption({ indexMeal: indexMeal, mealOption: mealOption, indexMealOption: indexMealOption }));
  
          this.dialog.open(ProjectComponent, {
              minHeight: '600px',
              maxHeight: '90%',
              minWidth: '1090px',
              maxWidth: '90%',
              disableClose: true,
              backdropClass: 'project-backdrop'
          }); */
    }

    getReport(templateDiet: DietModel): void {
        this.dialog.open(ReportDietComponent, {
            minHeight: '600px',
            maxHeight: '90%',
            minWidth: '1090px',
            maxWidth: '90%',
            disableClose: true,
            data: { templateDiet }
        });
    }

    onActiveMealOption(indexMeal: number, indexMealOption: number): void {
        this.store.dispatch(dietsActions.activeMealOption({ indexMeal: indexMeal, indexMealOption: indexMealOption }));
    }

    editDiet(): void {
        this.dialog.open(EditDietDialogComponent, {
            width: '1100px',
            disableClose: false,
            hasBackdrop: true,
            backdropClass: ['edit-diet-dialog-backdrop'],
            autoFocus: true,
            minHeight: '715px'
        });
    }
}
