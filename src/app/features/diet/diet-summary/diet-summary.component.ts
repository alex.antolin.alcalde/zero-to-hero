import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';
import { DietSummary } from '../store/diet.reducer';
import { selectDietSummaryState } from '../store/diet.selectors';

@Component({
    selector: 'z2h-diet-summary',
    templateUrl: './diet-summary.component.html',
    styleUrls: ['./diet-summary.component.scss']
})
export class DietSummaryComponent implements OnDestroy {

    onDestroy$ = new Subject<void>();
  
    dietSummary$: Observable<DietSummary> = this.store.select(selectDietSummaryState);

    constructor(
        private store: Store<fromApp.AppState>) {
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

}
