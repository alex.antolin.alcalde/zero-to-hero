import { Pipe, PipeTransform } from '@angular/core';
import { DietSummaryItem } from '../store/diet.reducer';

@Pipe({
    name: 'dietSummary'
})
export class DietSummaryPipe implements PipeTransform {

    transform(value: DietSummaryItem) {
        return value.current * 100 / value.goal;
    }
}