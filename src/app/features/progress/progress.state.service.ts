import { Injectable } from '@angular/core';
import { Chart } from '@antv/g2';
import { WeightHistoricModel } from '@core/models/user.model';
import { WeightHistoricComponent } from '@shared/components/weight-historic/weight-historic.component';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import Utils from 'src/app/utils/utils';

export enum WeightDateRange {
    '1W'= '1W',
    '2W' = '2W',
    '1M' = '1M',
    '3M' = '3M',
    '1Y' = '1Y',
}


@Injectable({
    providedIn: 'root'
})
export class ProgressState {

    private progress: WeightHistoricModel[];

    private chart: Chart;
    private weightDateRangeSelected = WeightDateRange['1Y'];
    private updating$ = new BehaviorSubject<boolean>(false);

    constructor() {}

    toggleWeightDateRange(weightDateRangeSelected: WeightDateRange): void {
        this.weightDateRangeSelected = weightDateRangeSelected;
        this.chart.data(this.getFilteredWeights(this.progress));
        this.chart.render();
    }

    buildChart(container: string, progress: WeightHistoricModel[]): void {
        
        if (!progress || progress.length === 0) {
            return;
        }
        this.progress = progress;

        this.chart = new Chart({
            container,
            autoFit: true,
            height: 280,
            padding: [ 20, 50, 60, 50 ]
        });

        this.chart.data(this.getFilteredWeights(this.progress));


        this.chart.tooltip({
            showCrosshairs: true,
            shared: false,
            title: (a,b) => moment(a).format(Utils.MOMENT_FORMAT)
        });

          
        this.chart.scale({
            date: {
                type: 'time',
                nice: true,
                tickInterval: 24 * 3600 * 1000 * (this.weightDateRangeSelected !== WeightDateRange['1W'] ? 3 : 1),
            },
            weight: {
                min: 79,
                max: 95,
                range: [0, 1],
            }
        });
        
          
        this.chart.axis('weight', {
            label: {
                formatter: (val) => val + ' Kg'
            },
        });

        this.chart.axis('date', {
            label: {
                autoRotate: false,
                rotate: 1,
                offset: 30,
                formatter: (val) => moment(val).format(Utils.MOMENT_FORMAT)
            },
        });
     
        
        this.chart.line()
            .color('#0d0c22')
            .position('date*weight')
            .shape('smooth');
        
        this.chart.point()
            .position('date*weight')
            .color('#ea4c89')
            .shape('circle');
        
        this.chart.render();

    }

    private getFilteredWeights(weightHistoric: WeightHistoricModel[]) {
        let startDate = moment();
        const endDate = moment();

        switch (this.weightDateRangeSelected) {
            case '1W':
                startDate = moment().subtract(1, 'weeks');
                break;
            case '2W':
                startDate = moment().subtract(2, 'weeks');
                break;
            case '1M':
                startDate = moment().subtract(1, 'months');
                break;
            case '3M':
                startDate = moment().subtract(3, 'months');
                break;
            case '1Y':
                startDate = moment().subtract(1, 'years');
        }

        const filteredWeights = weightHistoric.filter((weightH) => {
            const date = moment(weightH.date);
            return date.isBetween(startDate, endDate);
         /*    return moment(weightH.date) */
        });


        return filteredWeights;
    }
  
}