import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressComponent } from './progress.component';
import { SharedModule } from '@shared/shared.module';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        component: ProgressComponent
    }
];

@NgModule({
    declarations: [
        ProgressComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)]
})
export class ProgressModule {}
