import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { Chart } from '@antv/g2';
import { PersonalDataModel, WeightHistoricModel } from '@core/models/user.model';
import { Store } from '@ngrx/store';
import { selectUserPersonalState } from 'src/app/auth/state/auth.selectors';
import * as fromApp from 'src/app/store/app.reducer';
import Utils from 'src/app/utils/utils';

export type MacroChartData = { item: string, count: number, percent: number }[];

@Component({
	selector: 'app-progress',
	templateUrl: './progress.component.html',
	styleUrls: ['./progress.component.scss'],
	providers: [DatePipe],
	encapsulation: ViewEncapsulation.None
})
export class ProgressComponent implements AfterViewInit {
	chart: Chart;

	data: WeightHistoricModel[] = [];

	constructor(private store: Store<fromApp.AppState>) {

	}

	ngAfterViewInit() {
		this.store.select(selectUserPersonalState).subscribe((progress: PersonalDataModel) => {
			const fatsCount = (progress.fats * progress.weight * Utils.MACROS.fats.energy) * 100 / progress.kcal;
			const proteinsCount = (progress.proteins * progress.weight * Utils.MACROS.proteins.energy) * 100 / progress.kcal;

			const macroChartData: MacroChartData = [
				{ item: 'HC', count:~~(progress.hc), percent: +(progress.hc / 100).toFixed(2) },
				{ item: 'Grasas', count: ~~(fatsCount), percent:+(fatsCount / 100).toFixed(2) },
				{ item: 'Proteinas', count: ~~(proteinsCount), percent: +(proteinsCount / 100).toFixed(2) }
			];
			
			this.rednderMacrosChart(macroChartData);
		})
	}



	private rednderMacrosChart(macroChartData: MacroChartData): void {
		if (!macroChartData) {
			return;
		}

		const data = macroChartData;

		const chart = new Chart({
			container: 'macros-chart',
			autoFit: true,
			height: 350,
		});

		chart.data(data);

		chart.coordinate('theta', {
			radius: 0.85
		});

		chart.scale('percent', {
			formatter: (val) => {
				val = ~~(val * 100) + '%';
				return val;
			},
		});
		chart.tooltip({
			showTitle: false,
			showMarkers: false,
		});
		chart.axis(false); // 关闭坐标轴
		const interval = chart
			.interval()
			.adjust('stack')
			.position('percent')
			.color('item')
			.label('percent', {
				offset: -40,
				style: {
					textAlign: 'center',
					shadowBlur: 2,
					shadowColor: 'rgba(0, 0, 0, .45)',
					fill: '#fff',
				},
			})
			.tooltip('item*percent', (item, percent) => {
				percent = ~~(percent * 100) + '%';
				return {
					name: item,
					value: percent,
				};
			})
			.style({
				lineWidth: 2,
				stroke: '#fff',
			});
		chart.interaction('element-single-selected');
		chart.render();

		// 默认选择
		interval.elements[0].setState('selected', true);
	}

}
