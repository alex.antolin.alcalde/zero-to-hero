import { Injectable } from "@angular/core";
import { WeightHistoricModel } from "@core/models/user.model";
import { ProgressState, WeightDateRange } from "./progress.state.service";

@Injectable({
    providedIn: 'root'
})
export class ProgressFacade {

    constructor(private _progressState: ProgressState) { }

    buildChart(container: string, progress: WeightHistoricModel[]): void {
        this._progressState.buildChart(container, progress);
    }


    toggleWeightDateRange(weightDateRangeSelected: WeightDateRange): void {
        this._progressState.toggleWeightDateRange(weightDateRangeSelected);
    }

}