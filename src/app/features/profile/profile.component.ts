import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { PersonalDataModel, UserModel } from '@core/models/user.model';
import { ProfileActivityEnum, ProfileActiviyValueEnum, ProfileGoalEnum } from '@models/profile.model';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import Utils from 'src/app/utils/utils';
import * as fromApp from 'src/app/store/app.reducer';
import * as authActions from 'src/app/auth/state/auth.actions';
import { selectUserState } from 'src/app/auth/state/auth.selectors';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {

    name: string;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    BMR: number;
    TMB: number;
    TDEE: number;

    goalTypes = ProfileGoalEnum;
    activityTypes = ProfileActivityEnum;
    activityFactorValue = ProfileActiviyValueEnum;

    user: UserModel;

    get weight(): number { return this.firstFormGroup.get('weight').value; }
    get age(): number { return this.firstFormGroup.get('age').value; }
    get length(): number { return this.firstFormGroup.get('length').value; }
    get gender(): 'male' | 'female' { return this.firstFormGroup.get('gender').value; }
    get goal(): ProfileGoalEnum { return this.firstFormGroup.get('goal').value; }
    get kcal(): number { return this.secondFormGroup.get('kcal').value; }
    get hc(): number { return this.secondFormGroup.get('hc').value; }
    get fats(): number { return this.secondFormGroup.get('fats').value; }
    get proteins(): number { return this.secondFormGroup.get('proteins').value; }
    get activityFactor(): any { return this.secondFormGroup.get('activityFactor').value; }


    constructor(
        private store: Store<fromApp.AppState>,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<ProfileComponent>) {
    }

    getKeys(obj: any) { return Object.keys(obj); }


    ngOnInit() {
        this.buildForms();

        this.store.select(selectUserState).subscribe(
            (user: UserModel) => {
                this.user = user;

                // Patch value with user data
                if (user.personal) {
                    this.firstFormGroup.patchValue(user.personal);
                    this.secondFormGroup.patchValue(user.personal);
                    this.calculateTotalCalories();
                    this.updateMacrosForm();
                }
            }
        )

    }


    /**
     * Trigger when activity favtor input change.
     */
    onActivityFactorChanged(): void {
        this.calculateTotalCalories();
    }

    public onSelectionChange(event: StepperSelectionEvent) {

        const { selectedIndex } = event;

        if (selectedIndex === 1) {
            this.calculateTotalCalories();
        }
    }

    calculateTotalCalories(): void {
        // BMR
        this.BMR = Utils.BMR(this.gender,
            this.weight,
            this.length,
            this.age);

        // TDEE
        this.TDEE = Utils.TDEE(this.BMR, this.secondFormGroup.get('activityFactor').value);

        // GOAL
        this.secondFormGroup.get('kcal').setValue(~~Utils.getGoalCalories(this.TDEE, this.goal));
    }

    public async savePersonalData(stepper: MatStepper) {

        const personalData: PersonalDataModel = {
            ...this.firstFormGroup.value,
            ...this.secondFormGroup.value
        }

        this.store.dispatch(authActions.updatePersonalData({personalData: personalData}));

    }


    private buildForms() {
        // First
        this.firstFormGroup = this.fb.group({
            gender: ['male', Validators.required],
            age: [null, Validators.required],
            length: [null, Validators.required],
            weight: [null, Validators.required],
            bmi: [null],
            bodyFat: [null],
            goal: [null, Validators.required],

        });

        // Second [fake http request stepper]
        this.secondFormGroup = this.fb.group({
            activityFactor: [null, Validators.required],
            x: ['', Validators.required],
            kcal: [0, [Validators.min(0), Validators.required]],
            hc: [100, [Validators.min(0), Validators.max(100), Validators.required]],
            fats: [0, [Validators.min(0), Validators.required]],
            proteins: [0, [Validators.min(0), Validators.required]]
        });

        const updateMacrosForm = () => {
            const proteinsPercent = (this.secondFormGroup.get('proteins').value * this.weight) * Utils.MACROS.proteins.energy * 100 / this.kcal;
            const fatsPercent = (this.fats * this.weight) * Utils.MACROS.fats.energy * 100 / this.kcal;


            const restPercent = 100 - proteinsPercent - fatsPercent;

            this.secondFormGroup.get('hc').setValue(+(restPercent).toFixed(2));
        }

        // TODO: Test
        this.secondFormGroup.controls['kcal'].valueChanges.subscribe(updateMacrosForm);
        this.secondFormGroup.controls['fats'].valueChanges.subscribe(updateMacrosForm);
        this.secondFormGroup.controls['proteins'].valueChanges.subscribe(updateMacrosForm);

    }

    private updateMacrosForm() {
        const proteinsPercent = (this.secondFormGroup.get('proteins').value * this.weight) * Utils.MACROS.proteins.energy * 100 / this.kcal;
        const fatsPercent = (this.fats * this.weight) * Utils.MACROS.fats.energy * 100 / this.kcal;

        const restPercent = 100 - proteinsPercent - fatsPercent;

        this.secondFormGroup.get('hc').setValue(+(restPercent).toFixed(2));
    }

}
