import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserModel } from '@core/models/user.model';
import { ChangelogService } from '@core/services/changelog.service';
import { ChangelogComponent } from '@shared/components/changelog/changelog.component';
import { Observable } from 'rxjs';
import { config } from 'src/config/global';
import { ProfileComponent } from '../../profile/profile.component';
import * as fromApp from 'src/app/store/app.reducer';
import { Store } from '@ngrx/store';
import { selectUserState } from 'src/app/auth/state/auth.selectors';


@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent {

    DEBUG_MODE: boolean = config.DEBUG_MODE;
    user$: Observable<UserModel>;

    constructor(
    private store: Store<fromApp.AppState>,
    private dialog: MatDialog,
    private changelogService: ChangelogService) {
    this.user$ = this.store.select(selectUserState);
}

     /**
     * Open profile info dialog
     */
    public openProfileDialog(): void {
    this.dialog.open(ProfileComponent, {
        width: '890px',
/*         backdropClass: 'profile-backdrop', */
        disableClose: true
    });
}

    /**
     * Open changelog dialog
     */
    public openChangelog(): void {
    this.dialog.open(ChangelogComponent, {
        width: '680px',
        backdropClass: 'changelog-backdrop',
        data: this.changelogService.changelog
    });
}

     /**
     * Sign out
     */
    public signOut(): void {
    }
}
