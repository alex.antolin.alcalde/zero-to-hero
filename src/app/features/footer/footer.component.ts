import { Component, EventEmitter, Input, Output, ViewEncapsulation } from "@angular/core";
import { UserModel } from '@core/models/user.model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectUserState } from "src/app/auth/state/auth.selectors";
import * as fromApp from 'src/app/store/app.reducer';

export interface Tile {
    color: string;
    cols: number;
    rows: number;
    text: string;
  }

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FooterComponent{

    tiles: Tile[];
    @Input() themeActived: string = '';
    @Output() langChanged = new EventEmitter<string>();
    @Output() themeChanged = new EventEmitter<string>();
   
    user$: Observable<UserModel>;

    constructor(  private store: Store<fromApp.AppState>,) {
        this.user$ = this.store.select(selectUserState);

        this.tiles = [
            {text: 'dark', cols: 1, rows: 1, color: '#191b32'},
            {text: 'light', cols: 1, rows: 1, color: '#d6d6d6'}
        ];
    }

    onLangChange(lang: string) {
        this.langChanged.emit(lang);
    }

    onThemeChange(theme: string) {
        this.themeChanged.emit(theme);
    }
}