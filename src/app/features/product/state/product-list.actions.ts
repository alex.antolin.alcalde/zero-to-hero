import { GroupModel } from '@core/models/group.model';
import { ProductModel } from '@core/models/product.model';
import { createAction, props } from '@ngrx/store';

export const LOAD_PRODUCTS = '[Product] Load products';
export const SEARCH_PRODUCT = '[Product] Search product';
export const FILL_PRODUCTS = '[Product] Fill products';
export const CREATE_PRODUCT = '[Product] Create product';
export const CREATE_PRODUCT_SUCCESS = '[Product] Create product success';
export const MODIFY_PRODUCT = '[Product] Modify product';
export const MODIFY_PRODUCT_SUCCESS = '[Product] Modify product success';
export const DELETE_PRODUCTS = '[Product] Delete products';
export const DELETE_PRODUCTS_SUCCESS = '[Product] Delete products success';
export const SELECT_PRODUCTS = '[Product] Select products';
export const SELECT_PRODUCT_TO_EDIT = '[Product] Select product to edit';
export const ADD_PRICE = '[Product] Add price';
export const ADD_PRICE_SUCCESS = '[Product] Add price success';
export const SET_DISPLAYED_COLUMNS = '[Product] Set displayed columns';
export const AUTO_FILL_FILTERS = '[Product] Auto fill filters';
export const CHANGE_MASS = '[Product] Change mass';
export const CREATE_GROUP = '[Product] Create group';
export const CREATE_GROUP_SUCCESS = '[Product] Create group success';
export const GET_MACROS_BY_PRODUCT_ID = '[Product] Get macros by product id';

export const loadProducts = createAction(
    LOAD_PRODUCTS
)

export const fillProducts = createAction(
    FILL_PRODUCTS,
    props<{ products: ProductModel[] }>()
)

export const searchProductByName = createAction(
    SEARCH_PRODUCT,
    props<{ input: string }>()
)

export const  getMacrosByProductID= createAction(
    GET_MACROS_BY_PRODUCT_ID,
    props<{ id: string }>()
)

export const createProduct = createAction(
    CREATE_PRODUCT,
    props<{ product: ProductModel }>()
)

export const createProductSuccess = createAction(
    CREATE_PRODUCT_SUCCESS,
    props<{ product: ProductModel }>()
)

export const modifyProduct = createAction(
    MODIFY_PRODUCT,
    props<{ product: ProductModel }>()
)

export const modifyProductSuccess = createAction(
    MODIFY_PRODUCT_SUCCESS,
    props<{ product: ProductModel }>()
)

export const deleteProducts = createAction(
    DELETE_PRODUCTS
)

export const deleteProductsSuccess = createAction(
    DELETE_PRODUCTS_SUCCESS,
    props<{ productIDs: string[] }>()
)

export const selectProducts = createAction(
    SELECT_PRODUCTS,
    props<{ products: ProductModel[] }>()
)

export const selectProductToEdit = createAction(
    SELECT_PRODUCT_TO_EDIT,
    props<{ product: ProductModel }>()
)

/* export const addPrice = createAction(
    ADD_PRICE,
    props<{ price: PriceModel }>()
)

export const addPriceSuccess = createAction(
    ADD_PRICE_SUCCESS,
    props<{ prices: PriceModel[] }>()
)
 */
export const setDisplayedColumns = createAction(
    SET_DISPLAYED_COLUMNS,
    props<{ displayedColumns: string[], saveToStorage?: boolean }>()
)

export const autoFillFilters = createAction(
    AUTO_FILL_FILTERS
);

export const changeMass = createAction(
    CHANGE_MASS,
    props<{ mass: number, product: ProductModel }>()
)

export const createGroup = createAction(
    CREATE_GROUP,
    props<{ groupName: string }>()
)

export const createGroupSuccess = createAction(
    CREATE_GROUP_SUCCESS,
    props<{ group: GroupModel }>()
)
