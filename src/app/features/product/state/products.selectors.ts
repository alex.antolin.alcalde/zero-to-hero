import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromProductsReducer from './product-list.reducer';

export const selectProductsState = createFeatureSelector<fromProductsReducer.State>('productList');

export const selectProductListState = createSelector(
  selectProductsState,
  (state: fromProductsReducer.State) => state.productList
);

export const selectDisplayedColumnsState = createSelector(
  selectProductsState,
  (state: fromProductsReducer.State) => state.displayedColumns
);

export const selectProductsSelectedState = createSelector(
  selectProductsState,
  (state: fromProductsReducer.State) => state.productsSelected
);
