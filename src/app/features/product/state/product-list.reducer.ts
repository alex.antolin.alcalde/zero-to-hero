import { ProductModel } from "@core/models/product.model";
import { createReducer, on } from "@ngrx/store";
import { cloneDeep } from 'lodash';
import { filterItemsByName } from "src/app/utils/utils";
import * as productsActions from './product-list.actions';

export interface State {
    productsLoaded: ProductModel[];
    productList: ProductModel[];
    productsSelected: ProductModel[];
    inputSearch: string;
    editProduct: ProductModel;
    displayedColumns: string[];
}

const initialState: State = {
    productsLoaded: [],
    productList: [],
    productsSelected: [],
    inputSearch: '',
    editProduct: null,
    displayedColumns: ['name', 'brand', 'mass', 'calories', 'hc', 'fats', 'proteins', 'price']
}

const _productsReducer = createReducer(initialState,
    on(productsActions.fillProducts, (state, { products }) => ({
        ...state,
        productsLoaded: [...products],
        productList: [...products],
        productsSelected: []
    })),
    on(productsActions.selectProducts, (state, { products }) => ({
        ...state,
        productsSelected: [...products]
    })),
    on(productsActions.searchProductByName, (state, { input }) => {
        const productsFiltered = filterItemsByName(state.productsLoaded, input) as ProductModel[];

        return {
            ...state,
            productList: productsFiltered,
            inputSearch: input
        }
    }),
    on(productsActions.createProductSuccess, (state, { product }) => {
        const productsUpdated = [...state.productsLoaded, product];

        const productsFiltered = filterItemsByName(productsUpdated, state.inputSearch) as ProductModel[];

        return {
            ...state,
            productsLoaded: [...productsUpdated],
            productList: [...productsFiltered],
            productsSelected: []
        }
    }),
    on(productsActions.modifyProductSuccess, (state, { product }) => {
        const productsUpdated = state.productsLoaded.map(p => {
            if (p.id === product.id) {
                p = product;
            }
            return p;
        });

        const productsFiltered = filterItemsByName(productsUpdated, state.inputSearch) as ProductModel[];

        return {
            ...state,
            productsLoaded: [...productsUpdated],
            productList: [...productsFiltered],
            productsSelected: []
        }
    }),
    on(productsActions.deleteProductsSuccess, (state, { productIDs }) => {

        const productsUpdated = state.productsLoaded.filter(p => !productIDs.includes(p.id));

        const productsFiltered = filterItemsByName(productsUpdated, state.inputSearch) as ProductModel[];

        return {
            ...state,
            productsLoaded: [...productsUpdated],
            productList: [...productsFiltered],
            productsSelected: []
        }
    }),
    on(productsActions.selectProductToEdit, (state, { product }) => ({
        ...state,
        editProduct: product
    })),
    on(productsActions.setDisplayedColumns, (state, { displayedColumns }) => ({
        ...state,
        displayedColumns: [...displayedColumns]
    })),
    on(productsActions.changeMass, (state, { mass, product }) => {
       /*  const productsSelectedToUpdate = cloneDeep(state.productsSelected).map((prod: ProductModel) => {
            prod.originalMacros = prod.originalMacros ||{ ...prod.macros };

            if (prod.id === product.id) {
                prod.macros.mass = mass;
                prod.macros.calories = Utils.ruleOfThree(prod.originalMacros.calories, prod);
                prod.macros.hc = Utils.ruleOfThree(prod.originalMacros.hc, prod);
                prod.macros.fats = Utils.ruleOfThree(prod.originalMacros.fats, prod);
                prod.macros.proteins = Utils.ruleOfThree(prod.originalMacros.proteins, prod);
            }
            return prod;
        }); */

        return {
            ...state,
          /*   productsSelected: [...productsSelectedToUpdate] */
        }
    }),
)

export function productsReducer(state, action) {
    return _productsReducer(state, action)
}
