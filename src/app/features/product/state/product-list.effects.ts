import { Injectable } from '@angular/core';
import { ProductModel } from '@core/models/product.model';
import { ApiProductService } from '@core/services/api/api-product.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, exhaustMap, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import * as productsActions from '@features/product/state/product-list.actions';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import * as fromApp from 'src/app/store/app.reducer';
import { GroupModel } from '@core/models/group.model';
import { ApiGroupService } from '@core/services/api/api-group.service';
import * as moment from 'moment';

@Injectable()
export class ProductEffects {

    loadProducts$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.loadProducts),
        switchMap(() => this.apiProduct.getProducts().pipe(
            map(result => {
                return productsActions.fillProducts({ products: ProductModel.deserializeArray(result, ProductModel) });
            }),
            catchError(() => EMPTY)
        ))
    ));

    createProduct$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.createProduct),
        switchMap((action) => {
            const formData = new FormData();
            if (action.product.imageFile) {
                formData.append('file', action.product.imageFile, action.product.imageFile.name);
            }
            formData.append('body', JSON.stringify({
                name: action.product.name,
                brand: action.product.brand,
                type: action.product.type,
                macros: action.product.macros
            }));

            return this.apiProduct.createProduct(formData).pipe(
                map((productCreated) => {
                    const newProduct = ProductModel.deserializeObject(productCreated, ProductModel);
/*                     this.notifier.notify('success', this.translateService.instant('message.success.product-created')); */
                    return productsActions.createProductSuccess({ product: newProduct });
                }),
                catchError(() => EMPTY)
            )
        })
    ))

    modifyProduct$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.modifyProduct),
        switchMap((action) => {
            const formData = new FormData();
            if (action.product.imageFile) {
                formData.append('file', action.product.imageFile, action.product.imageFile.name);
            }
            formData.append('body', JSON.stringify({
                name: action.product.name,
                brand: action.product.brand,
                type: action.product.type,
                macros: action.product.macros,
                storeId: action.product.storeId
            }));

            return this.apiProduct.modifyProduct(action.product.id, formData).pipe(
                map((productUpdated) => {
                    const updateProduct = ProductModel.deserializeObject(productUpdated, ProductModel);
/*                     this.notifier.notify('success', this.translateService.instant('message.success.product-updated')); */
                    return productsActions.modifyProductSuccess({ product: updateProduct });
                }),
                catchError(() => EMPTY)
            )
        })
    ))


    deleteProducts$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.deleteProducts),
        withLatestFrom(this.store$.select('productList')),
        switchMap(([action, productsState]) => {
            const ids = productsState.productsSelected.map(p => p.id);

            return this.apiProduct.deleteProducts(ids).pipe(
                map(() => {
/*                     this.notifier.notify('success', this.translateService.instant('message.success.products-removed')); */
                    return productsActions.deleteProductsSuccess({ productIDs: ids });
                }),
                catchError(() => EMPTY)
            )
        })
    ))

    filteredColumns$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.setDisplayedColumns),
        tap((action) => {
            if (action.saveToStorage) {
                localStorage.setItem('productColumns', JSON.stringify(action.displayedColumns));
            }
        })
    ), { dispatch: false })

    autoFillFilters$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.autoFillFilters),
        map(() => {
            // Columns filter
            const productColumnsStorage = localStorage.getItem('productColumns');
            if (productColumnsStorage) {
                this.store$.dispatch(productsActions.setDisplayedColumns({ displayedColumns: JSON.parse(productColumnsStorage) }));
            }
        })
    ), { dispatch: false })

    createGroup$ = createEffect(() => this.actions$.pipe(
        ofType(productsActions.createGroup),
        withLatestFrom(this.store$.select('productList')),
        switchMap(([action, productsState]) => {
            const group: GroupModel = new GroupModel();
            group.name = action.groupName;
            group.foods = productsState.productsSelected;

            return this.apiGroup.createGroup(group).pipe(
                map((groupCreated) => {
                    const newGroup = GroupModel.deserializeObject(groupCreated, GroupModel);
/*                     this.notifier.notify('success', this.translateService.instant('message.success.group-created')); */
                    return productsActions.createGroupSuccess({ group: newGroup });
                }),
                catchError(() => EMPTY)
            )
        })
    ))

    constructor(private store$: Store<fromApp.AppState>, private actions$: Actions, private apiGroup: ApiGroupService, private apiProduct: ApiProductService, private translateService: TranslateService) { }
}
