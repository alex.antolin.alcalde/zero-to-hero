import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductModel } from '@models/product.model';
import { Store } from '@ngrx/store';
import * as productsActions from '@features/product/state/product-list.actions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as fromApp from '../../store/app.reducer';


@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {

    dataSource$: Observable<ProductModel[]>; 

    constructor(private store: Store<fromApp.AppState>) {
        this.dataSource$ = this.store.select('productList').pipe((map((state) => state.productList)));
    }

    ngOnInit() {
        this.store.dispatch(productsActions.autoFillFilters());
        this.store.dispatch(productsActions.loadProducts());
    }

    ngOnDestroy() {
        this.store.dispatch(productsActions.selectProducts({ products: [] }));
    }

    onSelectChange(products: ProductModel[]): void {
        this.store.dispatch(productsActions.selectProducts({ products: products }));
    }
    
}
