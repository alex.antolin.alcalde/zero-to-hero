import { Injectable } from '@angular/core';
import { ProductModel } from '@core/models/product.model';
import { ApiProductService } from '@core/services/api/api-product.service';
import { Store } from '@ngrx/store';
import * as _moment from 'moment';
import { Observable } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';
import * as productsActions from './state/product-list.actions';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {

    constructor(private apiProduct: ApiProductService, private store: Store<fromApp.AppState>) { }

    createProduct(product: ProductModel) {
        this.store.dispatch(productsActions.createProduct({ product: product }));
    }

    modifyProduct(product: ProductModel): void {
        this.store.dispatch(productsActions.modifyProduct({ product: product }));
    }

 /*    addPrice(form: any): void {
        form.date = _moment(form.date).format('YYYY-MM-DDTHH:mm[Z]');
        const price: PriceModel = PriceModel.deserializeObject(form, PriceModel);

        this.store.dispatch(productsActions.addPrice({ price: price }));
    } */


    searchOnMercadona(keyword): Observable<any[]> {
        return this.apiProduct.searchOnMercadona(keyword);
    }

    getMacrosByProductID(id: string): Observable<{
        kcal: number,
        hcs: number,
        proteins: number,
        fats: number
    }> {
        return this.apiProduct.getMacrosByProductID(id);
    }

}
