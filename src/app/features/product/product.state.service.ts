import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ProductModel } from '@core/models/product.model';
import Utils from 'src/app/utils/utils';
import { Summary } from '@core/models/summary.model';
import { GroupModel } from '@core/models/group.model';

@Injectable({
    providedIn: 'root'
})
export class ProductState {
    private allProducts: ProductModel[];
    private allGroups: GroupModel[];
    private lasInputFiltered: string = '';

    private updating$ = new BehaviorSubject<boolean>(false);
    private dataSource$ = new BehaviorSubject<ProductModel[]>([]);
    private groups$ = new BehaviorSubject<GroupModel[]>([]);
    private productsSelected$ = new BehaviorSubject<ProductModel[]>([]);
    private summaryGroupProducts$ = new BehaviorSubject<Summary>(null);

    isUpdating$() {
        return this.updating$.asObservable();
    }

    setUpdating(isUpdating: boolean) {
        this.updating$.next(isUpdating);
    }
    
    getDatasource$() {
        return this.dataSource$.asObservable();
    }

    getGroups$() {
        return this.groups$.asObservable();
    }

    getProductsSelected$() {
        return this.productsSelected$.asObservable();
    }
    
    getSummaryGroupProducts$() {
        return this.summaryGroupProducts$.asObservable();
    }

    setProducts(products: ProductModel[]) {
        this.allProducts = products;
        this.dataSource$.next(products);
    }

    setGroupProducts(groups: GroupModel[]) {
        this.allGroups = groups;
        this.groups$.next(groups);
    }

    addProduct(productCreated: ProductModel): void {
       /*  this.allProducts = [...this.allProducts, productCreated];

        let products = this.dataSource$.getValue();
    
        // If filtered => insert element to OUT and resort by name
        const mustBeInserted = Utils.normalizeString(productCreated.name.toLocaleLowerCase())
                    .includes(Utils.normalizeString(this.lasInputFiltered.toLocaleLowerCase())); 

        if (mustBeInserted) {
            products = [...products, productCreated];
        }
        products.sort((a, b) => a.name.localeCompare(b.name))
        
        this.dataSource$.next(products); */
    }
    
    setSelectedProducts(selectedProducts: ProductModel[]) {
        this.productsSelected$.next([...selectedProducts]);
    }


    changeMass(prodGroupToUpdate: ProductModel, newMass: number): void {
       /*  const productsGroup = this.productsSelected$.getValue();
        // Get element by id
        const product = productsGroup.find((prod: ProductModel) => prod.id === prodGroupToUpdate.id);
        
        // Save original macros
        product.originalMacros = product.originalMacros ||{ ...product.macros };

        // Update
        product.macros.mass = newMass;
        product.macros.calories = Utils.ruleOfThree(product.originalMacros.calories, product);
        product.macros.hc = Utils.ruleOfThree(product.originalMacros.hc, product);
        product.macros.fats = Utils.ruleOfThree(product.originalMacros.fats, product);
        product.macros.proteins = Utils.ruleOfThree(product.originalMacros.proteins, product);

        this.productsSelected$.next(productsGroup); */
    }

}