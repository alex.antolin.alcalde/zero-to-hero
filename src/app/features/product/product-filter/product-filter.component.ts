import { Component, ViewEncapsulation } from "@angular/core";
import { FormControl, Validators } from '@angular/forms';
import { MatOptionSelectionChange } from '@angular/material/core';
import { Summary } from '@core/models/summary.model';
import { Store } from "@ngrx/store";
import * as productsActions from '@features/product/state/product-list.actions';
import { Observable } from 'rxjs';
import * as fromApp from '../../../store/app.reducer';
import { selectDisplayedColumnsState } from "../state/products.selectors";
import { ProductsService } from "../products.service";
import { MatAutocompleteActivatedEvent, MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { CreateProductComponent } from "@shared/components/create-product/create-product.component";
import { Brands, ProductModel } from "@core/models/product.model";
import { MatDialog } from "@angular/material/dialog";

@Component({
    selector: 'z2h-product-filter',
    templateUrl: './product-filter.component.html',
    styleUrls: ['./product-filter.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProductFilterComponent {

    searchByName: FormControl = new FormControl(null);
    typeFilter: FormControl = new FormControl('products', Validators.required);
    columnsToDisplay: FormControl = new FormControl([]);

    summary$: Observable<Summary>;
    mercadonaKeyword: string;
    filteredOptions$: Observable<any[]>;
    columnsData = [
        { id: 'name', name: 'Name', checked: false },
        { id: 'brand', name: 'Brand', checked: false },
        { id: 'mass', name: 'Mass', checked: false },
        { id: 'calories', name: 'Calories', checked: false },
        { id: 'hc', name: 'HCs', checked: false },
        { id: 'fats', name: 'Fats', checked: false },
        { id: 'proteins', name: 'Proteins', checked: false },
        { id: 'price', name: 'Price', checked: false }
    ];

    constructor(
        private dialog: MatDialog,
        private store: Store<fromApp.AppState>,
        private productsService: ProductsService) {
        this.searchByName.valueChanges.subscribe((input: string) => {
            console.log("🚀 ~ file: product-filter.component.ts:47 ~ ProductFilterComponent ~ this.searchByName.valueChanges.subscribe ~ input:", input)
            
            this.store.dispatch(productsActions.searchProductByName({ input: input }))

        });
        this.store.select(selectDisplayedColumnsState).subscribe((columns: string[]) => {
            this.columnsToDisplay = new FormControl(columns);
        });
    }


    onSelectionChanged(event: MatOptionSelectionChange): void {
        if (event.isUserInput) {
        }
    }

    onSelectChange(event: any): void {
        const columnsToDisplay = event.value;
        this.store.dispatch(productsActions.setDisplayedColumns({
            displayedColumns: columnsToDisplay,
            saveToStorage: true
        }));
    }

    searchByKeyword(): void {
        this.filteredOptions$ = this.productsService.searchOnMercadona(this.mercadonaKeyword);
    }

    onOptionSelected(event: MatAutocompleteSelectedEvent): void {
        const { id, name, image } = event.option.value;

        if (id) {
            this.productsService.getMacrosByProductID(id).subscribe(
                (macros: {
                    kcal: number,
                    hcs: number,
                    proteins: number,
                    fats: number
                }) => {
                    const product: ProductModel = new ProductModel();
                    product.name = name;
                    product.brand = Brands.MERCADONA,
                        product.image = image;
                    product.macros = {
                        mass: 100,
                        massUnit: 'gr',
                        calories: macros.kcal,
                        fats: macros.fats,
                        proteins: macros.proteins,
                        hc: macros.hcs,
                        suggars: 0
                    }
                    this.dialog.open(CreateProductComponent, {
                        data: {
                            product
                        },
                        width: '780px',
                        disableClose: true
                    });
                });
            /*       this.store.dispatch(productsActions.getMacrosByProductID({ id })); */
        }
    }

    onOptionActivated(event: MatAutocompleteActivatedEvent): void {
    }
}
