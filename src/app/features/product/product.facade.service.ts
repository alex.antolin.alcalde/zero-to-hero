import { Injectable } from '@angular/core';
import { GroupModel } from '@core/models/group.model';
import { ProductModel } from '@core/models/product.model';
import { Summary } from '@core/models/summary.model';
import { Observable } from 'rxjs';
import { ProductState } from './product.state.service';

@Injectable({
    providedIn: 'root'
})
  export class ProductFacade {

    constructor(
    private productState: ProductState) { }

    getDatasource$(): Observable<ProductModel[]> {
        return this.productState.getDatasource$();
    }

    getGroups$(): Observable<GroupModel[]> {
        return this.productState.getGroups$();
    }

    getProductsSelected$() {
        return this.productState.getProductsSelected$();
    }

    getSummaryGroupProducts$(): Observable<Summary> {
        return this.productState.getSummaryGroupProducts$();
    }

    // [TODO]
    loadGroupProducts(): void {
        /* const user = this.auth.getUser();

        if (!user || !user.googleId) { return; }

        const params = new HttpParams()
          .set('userId', user.googleId);


        this.apiProductService.getGroupProducts({params})
        .pipe(
            map(result => GroupModel.deserializeArray(result, GroupModel))
        )
        .subscribe((products: GroupModel[]) => {
            this.productState.setGroupProducts(products);
        }) */
    }


    changeMass(prod: ProductModel, newMass: number): void {
        this.productState.changeMass(prod, newMass);
    }

    createGroup(group: any): void {
       /*  group.userId = this.auth.getUser().googleId;

        this.apiProductService.createGroup(group).pipe(
          map(result => {
            return GroupModel.deserializeObject(result, GroupModel);
          })
        ).subscribe((groupUpdated: GroupModel) => {
          if (groupUpdated) {
            this.notifier.notify('success', this.translateService.instant('message.success.group-created'));
          }
        }); */
    }

    setSelectedProducts(products: ProductModel[]): void {
        this.productState.setSelectedProducts(products);
    }
}
