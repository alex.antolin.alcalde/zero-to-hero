import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer) {
        this.registryIcons();
    }

    ngOnInit(): void {

    }

    private registryIcons() {
        this.matIconRegistry.addSvgIcon(
            `google-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/google-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `diet-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/diet-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `apple-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/apple-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `proteins-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/proteins-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `dumbbell-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/dumbbell-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `abs-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/abs-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `cycle-icon`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/cycle-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `meat`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/meat.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `bread`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/bread.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `egg`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/egg.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `fruit`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/fruit.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `vegetable`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/vegetable.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `cookie`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/cookie.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `rice`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/rice.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `pasta`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/pasta.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `fish`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/fish.svg')
        );
        this.matIconRegistry.addSvgIcon(
            `potato`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/potato.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `milk`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/milk.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `supp`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/supp.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `cereal`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/cereal.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `cheese`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/cheese.svg')
        );

        this.matIconRegistry.addSvgIcon(
            `carbohydrates`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/carbohydrates.svg')
        );
    }
}
