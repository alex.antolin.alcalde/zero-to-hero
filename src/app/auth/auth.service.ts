import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { UserModel } from '@core/models/user.model';
import { ApiAuthService } from '@core/services/api/api-auth.service';
import { Store } from '@ngrx/store';
import { browserSessionPersistence, getAuth, GoogleAuthProvider, OAuthCredential, setPersistence } from 'firebase/auth';
import { tap } from 'rxjs';
import * as authActions from 'src/app/auth/state/auth.actions';
import * as fromApp from 'src/app/store/app.reducer';

export interface GoogleUser {
  uid: string;
  email: string;
  photoURL: string;
  displayName: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private idToken: string;

  constructor(
    public afAuth: AngularFireAuth,
    private store: Store<fromApp.AppState>,
    private router: Router,
    private apiAuth: ApiAuthService,
  ) {}


  // Sign in with Google
  async GoogleAuth() {
    const provider = new GoogleAuthProvider();
    const auth = getAuth();
    setPersistence(auth, browserSessionPersistence).then(async() => {
      // Now sign-in using your chosen method.
      const { user: { uid, email, displayName, photoURL }, credential } = await this.afAuth.signInWithPopup(provider);
      this.idToken = (credential as OAuthCredential)?.idToken;


      return this.updateUserData({ uid, email, displayName, photoURL });
    })
  
  }

  async updateUserData(googleUser: GoogleUser) {

    this.apiAuth.saveUser(googleUser).pipe(
      tap(() => {
        this.router.navigate(['/']);
      })
    ).subscribe((user: UserModel) => {
      this.store.dispatch(authActions.setUser({ user: user }));
    }
    );
  }

  async signOut() {
    await this.afAuth.signOut();
    this.router.navigate(['/login']);
  }

  getIdToken(): string {
    return this.idToken;
  }

  setIdToken(idToken: string): void {
    this.idToken = idToken;
  }
}
