import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { CanActivate, Router } from '@angular/router';
import { ApiAuthService } from '@core/services/api/api-auth.service';
import { Store } from '@ngrx/store';
import { getAuth, User } from 'firebase/auth';
import { Observable, of, throwError } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';
import * as authActions from 'src/app/auth/state/auth.actions';
import * as fromApp from 'src/app/store/app.reducer';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private apiAuth: ApiAuthService,
    public afAuth: AngularFireAuth,
    private store: Store<fromApp.AppState>, private router: Router) { }

  canActivate(): Observable<boolean> {


    return this.afAuth.user.pipe(
      take(1),
      switchMap((user: User) => {
        if (user) {
          const { uid, email, displayName, photoURL } = user
          return this.apiAuth.saveUser({ uid, email, displayName, photoURL });
        }
        return of(null);
      }),
      switchMap(async (user) => {
        try {
          const auth = getAuth();
          const idToken = await auth.currentUser.getIdToken(true);
          this.authService.setIdToken(idToken);
      
          this.store.dispatch(authActions.setUser({ user: user }));
          return !!user;
        } catch(e) {
          throwError(() => new Error('User is null'));
          return false;
        };    
      }),
      tap(loggedIn => {
        if (!loggedIn) {
          this.router.navigate(['/login']);
        }
      })
    );
  }
}
