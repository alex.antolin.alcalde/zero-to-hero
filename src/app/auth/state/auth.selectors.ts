import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuthReducer from './auth.reducer';

export const selectAuthState = createFeatureSelector<fromAuthReducer.State>('auth');

export const selectUserState = createSelector(
  selectAuthState,
  (state: fromAuthReducer.State) => state.user
);

export const selectUserProgressState = createSelector(
  selectAuthState,
  (state: fromAuthReducer.State) => state.user.progress
);

export const selectUserPersonalState = createSelector(
  selectAuthState,
  (state: fromAuthReducer.State) => state.user.personal
);