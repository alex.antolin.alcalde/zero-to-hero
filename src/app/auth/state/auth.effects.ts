import { Injectable } from '@angular/core';
import { PersonalDataModel } from '@core/models/user.model';
import { ApiAuthService } from '@core/services/api/api-auth.service';
import * as dietsActions from '@features/diet/store/diet.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import * as authActions from 'src/app/auth/state/auth.actions';
import { selectUserState } from 'src/app/auth/state/auth.selectors';
import * as fromApp from 'src/app/store/app.reducer';

@Injectable()
export class AuthEffects {

    updatePersonalData$ = createEffect(() => this.actions$.pipe(
        ofType(authActions.updatePersonalData),
        withLatestFrom(this.store$.select(selectUserState)),
        switchMap(([action, user]) => {
            const personalData: PersonalDataModel = {
                ...action.personalData
            }
            return this.apiAuth.savePersonalData(user.id, personalData).pipe(
                mergeMap((userUpdated) => {
               /*      this.notifier.notify('success', this.translateService.instant('message.success.personal-data-updated')); */
                    return [
                        authActions.updatePersonalDataSuccess({ user: userUpdated }),
                        dietsActions.calculateDietSummary()
                    ]
                }),
                catchError(() => EMPTY)
            )
        })
    ))

    addWeight$ = createEffect(() => this.actions$.pipe(
        ofType(authActions.addWeight),
        withLatestFrom(this.store$.select(selectUserState)),
        switchMap(([action, user]) => {
            const weights = [...user.progress, action.weight];
            return this.apiAuth.addWeight(user.id, weights).pipe(
                map((userUpdated) => {
                /*     this.notifier.notify('success', this.translateService.instant('message.success.user-weight-updated')); */
                    return authActions.updatePersonalDataSuccess({ user: userUpdated })
                }),
                catchError(() => EMPTY)
            )
        })
    ))

    constructor(
        private store$: Store<fromApp.AppState>,
        private actions$: Actions,
        private apiAuth: ApiAuthService,
        private translateService: TranslateService) { }
}
