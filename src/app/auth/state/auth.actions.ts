import { PersonalDataModel, UserModel, WeightHistoricModel } from '@core/models/user.model';
import { createAction, props } from '@ngrx/store';

export const SET_USER = '[Auth] Set  user';
export const UPDATE_PERSONAL_DATA = '[Auth] Update personal data';
export const UPDATE_PERSONAL_DATA_SUCCESS = '[Auth] Update personal data success';
export const ADD_WEIGHT = '[Auth] Add weight';

export const setUser = createAction(
    SET_USER,
    props<{ user: UserModel }>()
)

export const updatePersonalData = createAction(
    UPDATE_PERSONAL_DATA,
    props<{ personalData: PersonalDataModel }>()
)

export const updatePersonalDataSuccess = createAction(
    UPDATE_PERSONAL_DATA_SUCCESS,
    props<{ user: UserModel }>()
)

export const addWeight = createAction(
    ADD_WEIGHT,
    props<{ weight: WeightHistoricModel }>()
)