import { UserModel } from "@core/models/user.model";
import { createReducer, on } from "@ngrx/store";
import * as authActions from './auth.actions';

export interface State {
    user: UserModel;
}

const initialState: State = {
    user: null
}

const _authReducer = createReducer(initialState,
    on(authActions.setUser, (state, { user }) => ({
        ...state,
        user: user
    })),
    on(authActions.updatePersonalDataSuccess, (state, { user }) => ({
        ...state,
        user: user
    })),
)

export function authReducer(state, action) {
    return _authReducer(state, action)
}
