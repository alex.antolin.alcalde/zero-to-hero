import { Pipe, PipeTransform } from '@angular/core';
import { MealModel } from '@core/models/meal.model';
import { ProductModel } from '@core/models/product.model';
import { Summary } from '@core/models/summary.model';

@Pipe({ name: 'summary' })
export class SummaryPipe implements PipeTransform {

    transform(data: MealModel | ProductModel[]): Summary {
        if (data instanceof MealModel){
            return null;
          /*   return data.getSummary(); */
        } else {
            let summary: Summary = {
                calories: 0,
                fats: 0,
                hcs: 0,
                proteins: 0,
            };

            data.forEach((product: ProductModel) => {
                summary.calories += product.macros.calories;
                summary.fats += product.macros.fats;
                summary.hcs += product.macros.hc;
                summary.proteins += product.macros.proteins;
            });
            return summary;
        }
    }
}
