import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import * as dietActions from '@features/diet/store/diet.actions';
import { Store } from "@ngrx/store";
import * as fromApp from 'src/app/store/app.reducer';

@Component({
    selector: 'z2h-modify-diet',
    templateUrl: './modify-diet.component.html',
})
export class ModifyDietComponent {

    constructor(
        private store: Store<fromApp.AppState>,
        public dialogRef: MatDialogRef<ModifyDietComponent>) { }

 
    save() {
        this.store.dispatch(dietActions.modifyDiet());
        this.close();
    }

    close() {
        this.dialogRef.close();
    }

  }
