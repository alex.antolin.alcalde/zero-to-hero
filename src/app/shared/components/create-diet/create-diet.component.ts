import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { DietService } from "@features/diet/diet.service";
import Utils from "src/app/utils/utils";


@Component({
    selector: 'create-diet',
    templateUrl: './create-diet.component.html',
    styleUrls: ['./create-diet.component.scss']
})
export class CreateDietComponent implements OnInit {
    form: FormGroup

    get name(): FormControl {
        return this.form.get('name') as FormControl;
    }

    get description(): FormControl {
        return this.form.get('description') as FormControl;
    }

    constructor(
        private dietService: DietService,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<CreateDietComponent>) { }

    ngOnInit() {
        this.buildForm();
    }

    onSubmit() {
        Utils.sanitizeDataBeforeSendToServer(this.form);

        const { name, description } = this.form.value;
        this.dietService.createDiet(name, description);


        this.dialogRef.close();
    }


    private buildForm() {
        this.form = this.fb.group({
            name: new FormControl('', Validators.required),
            description: new FormControl(''),
        });
    }

}
