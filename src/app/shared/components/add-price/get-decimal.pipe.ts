import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ 
    name: 'getDecimal'
})
export class GetDecimalPipe implements PipeTransform {
    
    transform(value: number) {
        return Math.floor((value%1)*100); 
    } 
}