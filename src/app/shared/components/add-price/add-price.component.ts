import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ProductsService } from '@features/product/products.service';
import { Store } from '@ngrx/store';
import * as _moment from 'moment';
import * as fromApp from 'src/app/store/app.reducer';

const moment =  _moment;
// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
const MY_FORMATS = {
    parse: {
      dateInput: 'DD/MM/YYYY',
    },
    display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'z2h-add-price',
    templateUrl: './add-price.component.html',
    styleUrls: ['./add-price.component.scss'],
      providers: [
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ],
})
export class AddPriceComponent implements OnInit {

    priceForm: FormGroup;


    constructor(
        private productsService: ProductsService,
        private store: Store<fromApp.AppState>,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<AddPriceComponent>) {
    }

    ngOnInit() {
        this.buildForm();
    }

    onSubmit() {
       /*  this.productsService.addPrice(this.priceForm.value); */
    }

    close() {
        this.dialogRef.close();
    }

    private buildForm() {
        this.priceForm = this.fb.group({
            quantity: [null , Validators.required],
            store: ['', Validators.required],
            date: [moment(), Validators.required],
            notes:[''],
        });
    }

}
