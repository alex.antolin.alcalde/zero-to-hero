import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { DietService } from "@features/diet/diet.service";

@Component({
    selector: 'add-meal',
    templateUrl: './add-meal.component.html',
    styleUrls: ['./add-meal.component.scss']
})
export class AddMealComponent {
    mealGroup = new FormGroup({
        name: new FormControl('', Validators.required)
    });

    get name(): string {
        return this.mealGroup.get('name').value;
    }

    constructor(
        private dietService: DietService,
        private dialogRef: MatDialogRef<AddMealComponent>) {}

    onSubmit() {
       this.dietService.addMealToDiet(this.name);
       this.dialogRef.close();
    }

  }
