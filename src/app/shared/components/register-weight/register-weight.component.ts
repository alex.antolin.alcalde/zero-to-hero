import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialogRef } from "@angular/material/dialog";
import { WeightHistoricModel } from '@core/models/user.model';
import * as moment from 'moment';
import * as fromApp from 'src/app/store/app.reducer';
import * as authActions from 'src/app/auth/state/auth.actions';
import { Store } from "@ngrx/store";

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
    parse: {
      dateInput: 'DD/MM/YYYY',
    },
    display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'register-weight',
    templateUrl: './register-weight.component.html',
    styleUrls: ['./register-weight.component.scss'],
    providers: [
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ]
})
export class RegisterWeightComponent {
    maxDate = moment().toDate();

    weightGroup = new FormGroup({
        date: new FormControl(this.currentDate, Validators.required),
        weight: new FormControl(null, Validators.required)

    });

    get currentDate() { return moment(); }

    get date(): any {
        return this.weightGroup.get('date').value;
    }

    get weight(): number {
        return this.weightGroup.get('weight').value;
    }

    constructor(
        private store: Store<fromApp.AppState>,
        private dialogRef: MatDialogRef<RegisterWeightComponent>) {}

    onSubmit() {
        const weightHistoric: WeightHistoricModel = {
            date: this.date.toDate(),
            weight: this.weight
        }

        this.store.dispatch(authActions.addWeight({ weight: weightHistoric}));
        this.dialogRef.close();
    }

  }
