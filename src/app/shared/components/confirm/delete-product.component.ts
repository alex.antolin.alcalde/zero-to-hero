import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { ProductModel } from "@core/models/product.model";
import * as productsActions from '@features/product/state/product-list.actions';
import { selectProductsSelectedState } from "@features/product/state/products.selectors";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromApp from '../../../store/app.reducer';


@Component({
    selector: 'app-delete-product',
    templateUrl: './delete-product.component.html'
})
export class DeleteProductComponent {

    productsToDelete$: Observable<ProductModel[]>

    constructor(
        private store: Store<fromApp.AppState>,
        public dialogRef: MatDialogRef<DeleteProductComponent>) {
        this.productsToDelete$ = this.store.select(selectProductsSelectedState);
    }

    deleteProducts() {
        this.store.dispatch(productsActions.deleteProducts());
        this.dialogRef.close();
    }
}
