import { Component, OnInit } from "@angular/core";


@Component({
    selector: 'page-not-found',
    templateUrl: './404.component.html',
    styleUrls: ['./404.component.scss']
})
export class Error404Component implements OnInit {

    constructor() {}

    ngOnInit() {

    }
}