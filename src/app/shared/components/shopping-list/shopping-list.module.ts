import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "@shared/material.module";
import { ShoppingListComponent } from "./shopping-list.component";

@NgModule({
    declarations: [
        ShoppingListComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
        TranslateModule
    ],
    exports: [
        ShoppingListComponent
    ]
})
export class ShoppingListModule {

}