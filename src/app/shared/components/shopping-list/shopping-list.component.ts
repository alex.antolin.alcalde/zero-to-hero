import { Component, OnDestroy, OnInit } from "@angular/core";
import { CustomProductModel } from "@core/models/product.model";
import * as dietsActions from '@features/diet/store/diet.actions';
import { selectShoppingListState } from "@features/diet/store/diet.selectors";
import { Store } from "@ngrx/store";
import { Observable, Subject } from "rxjs";
import * as fromApp from 'src/app/store/app.reducer';

export enum SHOPPING_LIST_GROUP_TYPE {
    MACRO,
    STORE,
}

export type productReferenceShoppingList = { p: CustomProductModel, i: number, g: string };

@Component({
    selector: 'z2h-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

    shoppingList$: Observable<{ [key: string]: CustomProductModel[] }> = this.store.select(selectShoppingListState);
    onDestroy$ = new Subject<void>();

    constructor(private store: Store<fromApp.AppState>) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    calculatePrices(shoppingList) {
        const allProducts: productReferenceShoppingList[] = [
            ...shoppingList.fats.map((p, i) => ({ p: p, i: i, g: 'fats'})),
            ...shoppingList.hcs.map((p, i) => ({ p: p, i: i, g: 'hcs'})),
            ...shoppingList.proteins.map((p, i) => ({ p: p, i: i, g: 'proteins'})),
            ...shoppingList.fruits.map((p, i) => ({ p: p, i: i, g: 'fruits'})),
            ...shoppingList.supp.map((p, i) => ({ p: p, i: i, g: 'supp'})),
        ]

        const products = allProducts.filter(pR => pR.p.item.storeId);

        this.store.dispatch(dietsActions.calculatePrices({ productReferenceList: products }))
       /*  cloneDeep(products).forEach(p => {
            this.getPriceByStoreId(p.item.brand, p.item.storeId).pipe(
                catchError((e) => {
                    console.warn(`No se ha podido conseguir el precio de ${p.item.name}`);
                    return EMPTY;
                })
            ).subscribe((price: Price) => {
                p.price = price;
            })
        }) */
    }

    
}