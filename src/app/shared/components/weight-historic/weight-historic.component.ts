import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { MatButtonToggleChange } from "@angular/material/button-toggle";
import { MatDialog } from "@angular/material/dialog";
import { ProgressFacade } from "@features/progress/progress.facade.service";
import { WeightDateRange } from "@features/progress/progress.state.service";
import * as moment from "moment";
import Utils from "src/app/utils/utils";
import { RegisterWeightComponent } from "../register-weight/register-weight.component";
import * as fromApp from 'src/app/store/app.reducer';
import { Store } from "@ngrx/store";
import { selectUserProgressState } from "src/app/auth/state/auth.selectors";
import { WeightHistoricModel } from "@core/models/user.model";

@Component({
    selector: 'weight-historic',
    templateUrl: './weight-historic.component.html',
    styleUrls: ['./weight-historic.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeightHistoricComponent implements OnInit {

    startDate: string = moment().subtract(1, 'weeks').format(Utils.MOMENT_FORMAT);
    endDate: string = moment().format(Utils.MOMENT_FORMAT);
    weightDateRange =  Object.values(WeightDateRange);

    constructor(
        private store: Store<fromApp.AppState>,
        private _dialog: MatDialog,
        private _progressFacade: ProgressFacade) {
        }

    ngOnInit() {}

    ngAfterViewInit() {
        this.store.select(selectUserProgressState).subscribe((progress: WeightHistoricModel[]) => {
            this._progressFacade.buildChart('weight-chart', progress);
        })
    }

    onWeightToggleChange(event: MatButtonToggleChange): void {
        const { value } = event;
        switch (value) {
            case '1W':
                this.startDate = moment().subtract(1, 'weeks').format(Utils.MOMENT_FORMAT);
                break;
            case '2W':
                this.startDate = moment().subtract(2, 'weeks').format(Utils.MOMENT_FORMAT);
                break;
            case '1M':
                this.startDate = moment().subtract(1, 'months').format(Utils.MOMENT_FORMAT);
                break;
            case '3M':
                this.startDate = moment().subtract(3, 'months').format(Utils.MOMENT_FORMAT);
                break;
            case '1Y':
                this.startDate = moment().subtract(1, 'years').format(Utils.MOMENT_FORMAT);
        }
        this._progressFacade.toggleWeightDateRange(value);
    }

    registerWeight() {
        this._dialog.open(RegisterWeightComponent, {
            width: '350px'
        });
    }

}