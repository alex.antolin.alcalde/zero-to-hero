import { Pipe, PipeTransform } from '@angular/core';
import { ProductModel } from '@core/models/product.model';

@Pipe({
    name: 'productType'
})
export class ProductTypePipe implements PipeTransform {

    transform(product: ProductModel, args?: any): any {
        let type = '';
        switch (product.type) {

            case 'carne':
                type = 'meat';
                break;
            case 'verdura':
                type = 'vegetable';
                break;
            case 'galleta':
                type = 'cookie';
                break;
            case 'fruta':
                type = 'fruit';
                break;
            case 'pan':
                type = 'bread';
                break;
            case 'arroz':
                type = 'rice';
                break;
            case 'pasta':
                type = 'pasta';
                break;
            case 'pescado':
                type = 'fish';
                break;
            case 'patata':
                type = 'potato';
                break;
            case 'leche':
                type = 'milk';
                break;
            case 'supp':
                type = 'supp';
                break;
            case 'cereal':
                type = 'cereal';
                break;
            case 'queso':
                type = 'cheese';
                break;
            case 'huevo':
                type = 'egg';
                break;
        }
        return type;
    }
}
