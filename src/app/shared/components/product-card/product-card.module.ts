import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { ClickOutsideDirective } from './click-outside.directive';
import { ProductCardComponent } from './product-card.component';
import { ProductTypePipe } from './product-type.pipe';
import { ProductNamePipe } from './product.name.pipe';

@NgModule({
    declarations: [
        ProductCardComponent,
        ProductTypePipe,
        ProductNamePipe,
        ClickOutsideDirective
    ],
    imports: [
        MatIconModule,
        CommonModule,
        FormsModule,
        MatMenuModule,
        MatFormFieldModule,
    ],
    exports: [
        ProductCardComponent,
        ProductTypePipe
    ]
})
export class ProductCardModule { }
