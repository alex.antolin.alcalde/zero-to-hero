import { Directive, ElementRef, EventEmitter, Output, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[clickOutside]'
})
export class ClickOutsideDirective {

    constructor(private elementRef: ElementRef) { }

    @Input() editMassMode: boolean;
    @Output() clickOutside = new EventEmitter<MouseEvent>();

    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {

        if (!targetElement) {
            return;
        }
        
        const isSpanElement = targetElement.id === 'unit-product';
        const clickedInside = this.elementRef.nativeElement.contains(targetElement);
        if (!isSpanElement && !clickedInside) {
            this.clickOutside.emit(event);
        }
    }
}