import { Pipe, PipeTransform } from '@angular/core';
import { ProductModel } from '@core/models/product.model';

@Pipe({
    name: 'productName'
})
export class ProductNamePipe implements PipeTransform {

    transform(product: ProductModel, args?: any): string {
        const vocals = ['a', 'e', 'i', 'o', 'u'];
        if (product.macros.massUnit !== 'gr' && product.macros.massUnit !== 'ml' && product.macros.mass > 1) {
            if (vocals.includes(product.name.slice(-1))) {
                return `${product.name}s`;
            } else {
                return `${product.name}es`;
            }
        }
        return product.name;
    }
}
