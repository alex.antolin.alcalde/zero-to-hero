import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener, OnInit } from '@angular/core';
import { CustomProductModel, ProductModel } from '@core/models/product.model';

@Component({
    selector: 'z2h-product-card',
    templateUrl: './product-card.component.html',
    styleUrls: ['./product-card.component.scss'],
    animations: [
        trigger('bodyExpansion', [
            state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
            state('expanded', style({ height: '*', visibility: 'visible' })),
            transition('expanded <=> collapsed, void => collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ])
    ]
})
export class ProductCardComponent implements OnInit {

    @Input() product: ProductModel;
    @Input() mass: number;
    @Input() fullDetails: boolean = false;
    @Output() editProduct = new EventEmitter<CustomProductModel>();
    @Output() deleteProduct = new EventEmitter<ProductModel>();
    state = 'collapsed';

    @ViewChild('editMassInput') editMassInput: ElementRef;
    editMassMode = false;
    pendingValue: number;

    ngOnInit(): void {
        this.mass ? this.mass : this.product.macros.mass;
        this.pendingValue = this.mass;
    }

    delete(): void {
        this.deleteProduct.emit(this.product);
    }

    toggle(): void {
        this.state = this.state === 'collapsed' ? 'expanded' : 'collapsed';
    }

    edit(): void {
        this.editMassMode = true;
        setTimeout(() => {
            this.editMassInput.nativeElement.focus();
        }, 0);

    }

    cancelEdit(): void {
        this.editMassMode = false;
    }

    processChanges(): void {
        this.editProduct.emit({
            item: this.product,
            mass: this.pendingValue
        });
        

        this.editMassMode = false;

    }
}
