import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})
export class UploadComponent {
    fileData: File;
    @Input() previewUrl: string = null;
    @Input() selection: 'single' | 'multiple' = 'single';
    @Output() fileUpload: EventEmitter<File> = new EventEmitter<File>();

    uploadFile(files: File[]) {
        if (!files || files.length <= 0 || !this.checkExtension(files[0]) ) { return null; };

        this.fileData = <File>files[0];
        this.preview();

        this.fileUpload.emit(this.fileData);
    }

    preview() {
        // Show preview 
        var mimeType = this.fileData.type;
  
        var reader = new FileReader();      
        reader.readAsDataURL(this.fileData); 
        reader.onload = (_event) => { 
          this.previewUrl = reader.result as string; 
        }
    }

    removeFile():void {
        this.previewUrl = null;
        this.fileData = null;
    }

    private checkExtension(file: File): boolean {
        const filetypes = /jpeg|jpg|png|gif/;
        const mimetype = filetypes.test(file.type);
        return mimetype;
    };
}