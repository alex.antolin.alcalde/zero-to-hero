import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GroupModel } from '@core/models/group.model';

@Component({
    selector: 'app-group-list',
    templateUrl: './group-list.component.html',
    styleUrls: ['./group-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class GroupListComponent {

    displayedColumnsGroup: string[] = ['name', 'brand', 'mass', 'calories', 'hc', 'fats', 'proteins'];

    @Input() dataSource: GroupModel[] = [];
    @Output() undo = new EventEmitter<any>();
    
    constructor(public dialog: MatDialog) {
    }

    isGroup(index, item): boolean{
        return item instanceof GroupModel;
    }

    undoGroup(item: GroupModel) {
        this.undo.emit(item);
    }

}
