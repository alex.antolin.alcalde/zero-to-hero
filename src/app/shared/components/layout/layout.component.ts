import { Component, Renderer2, ViewEncapsulation } from '@angular/core';
import { config } from 'src/config/global';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserModel } from '@core/models/user.model';
import { Observable } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';
import { Store } from '@ngrx/store';
import { selectAuthState, selectUserState } from 'src/app/auth/state/auth.selectors';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppLayoutComponent {
  DEBUG_MODE: boolean = config.DEBUG_MODE;
  themeActive = 'light';
  menuToShow = '';
  user$: Observable<UserModel> = this.store.select(selectUserState);
  activePath: string = '';

  constructor(
    private store: Store<fromApp.AppState>,
    private translate: TranslateService,
    private renderer: Renderer2,
    private readonly router: Router) {
    this.router.events.pipe(
      filter((e): e is NavigationEnd => e instanceof NavigationEnd)
    ).subscribe((val: NavigationEnd) => this.activePath = val.urlAfterRedirects.substring(1));

    if (this.DEBUG_MODE) console.warn('::DEBUG MODE::');


    // setting default theme
    this.themeActive.toLocaleLowerCase() === 'dark' ?
      // setting default theme
      this.renderer.addClass(document.body, 'hero-dark-theme') :
      // setting default theme
      this.renderer.removeClass(document.body, 'hero-dark-theme');

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('es');

  }

  /**
   * 
   * @param lang ,
    {
      path: 'dietsMenu',
      component: DietMenuComponent,
      outlet: 'sidebar'
    },
    {
      path: 'productsMenu',
      component: ProductMenuComponent,
      outlet: 'sidebar'
    }
   */

  onLangChange(lang: string) {
    this.translate.use(lang);
  }

  onThemeChange(theme: string) {
    theme.toLocaleLowerCase() === 'dark' ? this.renderer.addClass(document.body, 'hero-dark-theme') : this.renderer.removeClass(document.body, 'hero-dark-theme');
    this.themeActive = theme.toLocaleLowerCase();
  }

}