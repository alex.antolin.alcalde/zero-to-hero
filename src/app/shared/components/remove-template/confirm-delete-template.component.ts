import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { DietModel } from "@core/models/diet.model";
import { DietService } from "@features/diet/diet.service";

@Component({
    selector: 'vy-confirm-delete-template',
    templateUrl: 'confirm-delete-template.component.html',
})
export class ConfirmDeleteTemplateComponent {

    template: DietModel;

    constructor(
        private dietService: DietService,
        public dialogRef: MatDialogRef<ConfirmDeleteTemplateComponent>) {
    }

    close() {
        this.dialogRef.close();
    }

    confirm() {
        this.dietService.deleteDiet();
        this.dialogRef.close(true);
    }
}
