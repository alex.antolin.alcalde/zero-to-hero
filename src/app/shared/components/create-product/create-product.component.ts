import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Brands, ProductModel } from '@core/models/product.model';
import { environment } from '@env/environment';
import Utils from 'src/app/utils/utils';
import { ProductsService } from '../../../features/product/products.service';

@Component({
    selector: 'app-create-product',
    templateUrl: './create-product.component.html',
    styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
    form: FormGroup;
    editMode = false;
    file: File = null;
    image: string = null;
    Brands = Brands;

    get productImage(): string {
        return this.data?.product?.image || null;
    }

    constructor(
        private fb: FormBuilder,
        private productsService: ProductsService,
        public dialogRef: MatDialogRef<CreateProductComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { product: ProductModel }) { }

    ngOnInit() {
        this.buildForm();

        if (this.data && this.data.product) {
            this.editMode = true;
            this.form.patchValue(this.data.product);

            if (this.data.product.image) {
                this.image = `${environment.apiUrl}${this.data.product.image}`;
            }
        }
    }

    onSubmit() {
        Utils.sanitizeDataBeforeSendToServer(this.form);
        const productToSend = this.editMode ? {...this.data.product, ...this.form.value} : this.form.value;
        let product = ProductModel.deserializeObject(productToSend, ProductModel);
        
        if (this.file?.name) {
            product.imageFile = this.file;
        }
        
        if (this.editMode) {
            product.id = this.data.product.id;
            product.image = productToSend.image || null;
            this.productsService.modifyProduct(product);
        } else {
            this.productsService.createProduct(product);
        }

        this.dialogRef.close();
    }

    private buildForm() {
        this.form = this.fb.group({
            name: [null, Validators.required],
            storeId: [null],
            brand: [null],
            type: [null],
            macros: this.fb.group({
                mass: [100],
                massUnit: ['gr'],
                calories: ['0'],
                hc: [0],
                suggars: [0],
                fats: [0],
                proteins: [0]
            }),
        });
    }

    close() {
        // TODO: Show popup if not save
        this.dialogRef.close();
    }

    fileUploaded(file: File) {
        this.file = file;
    }
}
