import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import * as fromApp from 'src/app/store/app.reducer';
import * as dietsActions from '@features/diet/store/diet.actions';

@Component({
    selector: 'z2h-day-of-week-selector',
    templateUrl: './day-of-week-selector.component.html',
    styleUrls: ['./day-of-week-selector.component.scss']
})
export class DayOfWeekSelector  {

    days: string[] = [
        'days.sunday',
        'days.monday',
        'days.tuesday',
        'days.wednesday',
        'days.thursday',
        'days.friday',
        'days.saturday',
    ]
    currentDay: number =  new Date().getDay();

    constructor(  private store: Store<fromApp.AppState>) {}

    previous(): void {
        this.currentDay = this.days[--this.currentDay] === undefined ? this.days.length - 1 : this.currentDay--;
        this.dispatchDay(this.currentDay);
    }

    next(): void {
        this.currentDay = this.days[++this.currentDay] === undefined ? 0 : this.currentDay++;
        this.dispatchDay(this.currentDay);
    }

    dispatchDay(day: number) {
        this.store.dispatch(dietsActions.changeDayOfWeek({ day }));
    }
}