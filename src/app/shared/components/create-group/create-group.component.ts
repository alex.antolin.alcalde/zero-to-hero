import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ProductModel } from '@core/models/product.model';
import { selectProductsSelectedState } from '@features/product/state/products.selectors';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';
import * as productsActions from '@features/product/state/product-list.actions';

@Component({
    selector: 'z2h-create-group',
    templateUrl: './create-group.component.html'
})
export class CreateGroupComponent implements OnInit {

    form: FormGroup;
    products$: Observable<ProductModel[]>;

    constructor(
        private store: Store<fromApp.AppState>,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<CreateGroupComponent>) {

            this.products$ = this.store.select(selectProductsSelectedState);

    }

    ngOnInit() {
        this.buildForm();
    }

    onSubmit() {
        if (this.form.valid) {
            const groupName = this.form.get('name').value;
            this.store.dispatch(productsActions.createGroup({ groupName: groupName}));
            this.close();
        }
    }
    
    
    close() {
        this.dialogRef.close();
    }


    onMassChange(event: { product, newMass }) {
        const { product, newMass } = event;
 /*        this.lastInputMassChange = `input-mass-${product.id}`; */
        this.store.dispatch(productsActions.changeMass({ mass: newMass, product: product }));
    }

    private buildForm() {
        this.form = this.fb.group({
            name: [null, Validators.required]
        });
    }
}
