import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductFacade } from '@features/product/product.facade.service';
import { ProductModel } from '@models/product.model';
import { Store } from '@ngrx/store';
import { AddPriceComponent } from '../add-price/add-price.component';
import { CreateProductComponent } from '../create-product/create-product.component';
import * as fromApp from 'src/app/store/app.reducer';
import * as productsActions from '@features/product/state/product-list.actions';
import { selectDisplayedColumnsState } from '@features/product/state/products.selectors';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'z2h-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProductListComponent  {

    displayedColumns$: Observable<string[]> = this.store.select(selectDisplayedColumnsState).pipe(
        map((columnsToDisplay: string[]) => {
            const columns = [...columnsToDisplay];
            if (this.undoAction) {
                columns.unshift('undo');
            }
            if (this.selectionMode) {
                columns.unshift('select');
            }
            if (this.hasActions) {
                columns.push('actions')
            }
            return columns;
        })
    );
    
    @Input() dataSource: ProductModel[] = [];
    @Input() viewMode: 'small' | 'big' = 'small';
    @Input() editMode = false;
    @Input() selectionMode = false;
    @Input() hasActions = false;
    @Input() undoAction = false;
    @Output() massChanged = new EventEmitter<any>();
    @Output() undo = new EventEmitter<any>();
    

    selection: SelectionModel<ProductModel> = new SelectionModel<ProductModel>(true, []);


    // NEW 
    @Output() selectChanged = new EventEmitter<ProductModel[]>();

    constructor(
        private readonly store: Store<fromApp.AppState>,
        private productFacade: ProductFacade,
        public dialog: MatDialog) {

        this.productFacade.getProductsSelected$().subscribe((productsSelected: ProductModel[]) => {
            if (productsSelected.length !== this.selection.selected.length) {
                this.selection = new SelectionModel<ProductModel>(true, productsSelected);
            }
        });
    }

    onSelect(row) {
        this.selection.toggle(row);
        this.selectChanged.emit(this.selection.selected);
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.length;
        return numSelected === numRows;
    }

    undoProduct(item: ProductModel) {
        this.undo.emit(item);
    }

    editProduct(product: ProductModel) {
        this.dialog.open(CreateProductComponent, {
            data: {
                product
            },
            width: '780px',
            disableClose: true
        });
    }

    addPrice(product: ProductModel) {
        this.store.dispatch(productsActions.selectProductToEdit({ product: product}));

        this.dialog.open(AddPriceComponent, {
            width: '425px',
            height: '525px',
            disableClose: true,
            backdropClass: 'add-price-backdrop'
        });
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.forEach(row => this.selection.select(row));

        this.selectChanged.emit(this.selection.selected);
    }

    onMassChange(newMass: any, product: ProductModel) {
        this.massChanged.emit({product, newMass});
    }
}
