import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-changelog',
    templateUrl: './changelog.component.html',
    styleUrls: ['./changelog.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ChangelogComponent implements OnInit {
    changelogData: any;

    constructor( public dialogRef: MatDialogRef<ChangelogComponent>,
    @Inject(MAT_DIALOG_DATA) public changelog: any) {
        this.changelogData = changelog;
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }



}
