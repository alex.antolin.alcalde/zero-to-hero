import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { GroupModel } from "@core/models/group.model";
import { MealModel } from "@core/models/meal.model";
import { ProductModel } from "@core/models/product.model";
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: 'report-diet',
    templateUrl: './report-diet.component.html',
    styleUrls: ['./report-diet.component.scss']
})
export class ReportDietComponent implements OnInit {
    products: ProductModel[];
    days = 7;


    constructor(
        public dialogRef: MatDialogRef<ReportDietComponent>,
        @Inject(MAT_DIALOG_DATA) private data: { templateDiet }) {

    }

    ngOnInit() {
        this.mergeProducts();
    }

    onDaysChange(days: any): void {
        this.days = days;
        this.mergeProducts();

    }

    private mergeProducts(): void {
        /* let products = [];
        this.data.templateDiet.meals.forEach((meal: MealModel) => {
            products = [...products, ...cloneDeep(meal.foods)];
            meal.groups.forEach((group: GroupModel) => products = [...products, ...cloneDeep(group.foods)]);
        });

        
        console.log('FIRST STEP:: ', products);
        const allProducts = [];

        products.forEach((prod: ProductModel) => {
            const prodFound: ProductModel = allProducts.find(p => p.id === prod.id);
            if (!prodFound) {
                allProducts.push(prod);
            } else {
                prodFound.macros.mass += prod.macros.mass;
                prodFound.macros.calories += prod.macros.calories;
                prodFound.macros.fats += prod.macros.fats;
                prodFound.macros.hc += prod.macros.hc;
                prodFound.macros.proteins += prod.macros.proteins;
            }
        });

        console.log('SECOND STEP:: ', allProducts);


        allProducts.forEach((prod: ProductModel) => prod.macros.mass *= this.days);
        this.products = cloneDeep(allProducts);
        console.log('THIRD STEP:: ', this.products); */

    }

}