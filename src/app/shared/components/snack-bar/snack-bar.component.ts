import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
export interface SnackbarDATA {
    code?: number;
    message?: SafeHtml;
}
@Component({
    templateUrl: './snack-bar.component.html',
    styleUrls: ['./snack-bar.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SnackBarComponent {

    snackBarDATA: SnackbarDATA = {};

    constructor(
        @Inject(MAT_SNACK_BAR_DATA) public data: SnackbarDATA,
        private sanitizer: DomSanitizer) {
        this.snackBarDATA = data;
    }

}
