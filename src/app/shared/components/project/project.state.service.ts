import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MealModel } from '@core/models/meal.model';
import { ProductModel } from '@core/models/product.model';

import Utils from 'src/app/utils/utils';
import { Summary, SummaryMacro } from '@core/models/summary.model';
import { GroupModel } from '@core/models/group.model';


export interface DataSource {
    in: {
        products: Array<ProductModel>;
        groups: (GroupModel | ProductModel)[];
    };
    out: {
        products: Array<ProductModel>;
        groups: Array<GroupModel>;
    };
}

@Injectable()
export class ProjectState {
    private meal: MealModel;
    private lasInputFiltered: string = '';

    private allProducts: ProductModel[];
    private allGroupsproduct: GroupModel[];
    
    private updating$ = new BehaviorSubject<boolean>(false);
    private dataSource$ = new BehaviorSubject<DataSource>(null);
    private summary$ = new BehaviorSubject<Summary>(null);

    isUpdating$() {
        return this.updating$.asObservable();
    }

    setUpdating(isUpdating: boolean) {
        this.updating$.next(isUpdating);
    }

    setMeal(meal: MealModel) {
        this.meal = Object.assign({}, meal);
    }

    getSummary$() {
        return this.summary$.asObservable();
    }
    
    setSummary(summary: Summary) {
        return this.summary$.next({...summary})
    }
    
    getDatasource$() {
        return this.dataSource$.asObservable();
    }

    changeMass(prodToUpdate: ProductModel, newMass: number): void {
      /*   const dataSource = this.dataSource$.getValue();

        // Get element by id
        const product: ProductModel = dataSource.in.products.find((prod: ProductModel) => prod.id === prodToUpdate.id) as ProductModel;
        
        // Save original macros
        product.originalMacros = product.originalMacros ||{ ...product.macros };

        // Update
        product.macros.mass = newMass;
        product.macros.calories = Utils.ruleOfThree(product.originalMacros.calories, product);
        product.macros.hc = Utils.ruleOfThree(product.originalMacros.hc, product);
        product.macros.fats = Utils.ruleOfThree(product.originalMacros.fats, product);
        product.macros.proteins = Utils.ruleOfThree(product.originalMacros.proteins, product);

        this.dataSource$.next(dataSource); */
    }

    /**
     * Trigger when user input some text to filter products list OUT
     * @param input user input was typed
     */
    filterProductList(input: string) {
        const dataSource = this.dataSource$.getValue();

        this.lasInputFiltered = input;
        
        const filter = (prods: ProductModel[]) => {
            return prods.filter((prod: ProductModel) => {
                return Utils.normalizeString(prod.name.toLocaleLowerCase()).includes(Utils.normalizeString(input.toLocaleLowerCase()))
            });
        }

        const hasToExclude = (): ProductModel[] => {
            return this.allProducts.filter((p1: ProductModel) =>
            !dataSource.in.products.some((p2:  ProductModel) => p1.id === p2.id))
        }

        dataSource.out.products = filter((dataSource.in.products.length > 0 ? hasToExclude() : this.allProducts))

        this.dataSource$.next(dataSource);
    }

    /**
     * Trigger when user input some text to filter groups list OUT
     * @param input user input was typed
     */
    filterGroupList(input: string) {
        const dataSource = this.dataSource$.getValue();

        this.lasInputFiltered = input;
        
        const filter = (groups: GroupModel[]) => {
            return groups.filter((group: GroupModel) => {
                return Utils.normalizeString(group.name.toLocaleLowerCase()).includes(Utils.normalizeString(input.toLocaleLowerCase()))
            });
        }

        const hasToExclude = (): GroupModel[] => {
            return this.allGroupsproduct.filter((g1: GroupModel) =>
            !dataSource.in.groups.some((g2:  GroupModel) => g1.id === g2.id))
        }

        dataSource.out.groups = filter((dataSource.in.groups.length > 0 ? hasToExclude() : this.allGroupsproduct))

        this.dataSource$.next(dataSource);
    }


    calculateSummary() {
/*         const products: ProductModel[] = this.dataSource$.getValue().in.products as ProductModel[];
        const productsIngroups: ProductModel[] = this.dataSource$.getValue().in.groups.filter((item) => item instanceof ProductModel) as ProductModel[];
        const allProducts = [...products, ...productsIngroups];
        
        const summary = this.summary$.getValue();

        const current: SummaryMacro = Utils.getCurrentMacroByProducts(allProducts);
        summary.current = current;
        this.summary$.next(summary); */
    }

    private mapGroupsModelToDatasource(groups: GroupModel[]): (GroupModel | ProductModel)[]  {
        const ds =  groups.reduce((ds, group) => {
            return ds.concat([group, ...group.foods.map((product: ProductModel) => {
                product['groupId'] = group.id;
                return product;
            })]);
        }, []);

        return ds;
    }

}