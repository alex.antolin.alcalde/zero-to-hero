import { Component, ViewEncapsulation } from "@angular/core";
import { ProjectFacade } from './project.facade.service';
import { ProjectState } from './project.state.service';

@Component({
    selector: 'project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [ProjectFacade, ProjectState]
})
export class ProjectComponent  {

    /* onDestroy$ = new Subject<void>();

    mealOption$: Observable<MealOptionModel>;
    tabSelected: number = 0;

    productFilter: FormControl = new FormControl(null);
    groupFilter: FormControl = new FormControl(null);

    productList$: Observable<ProductModel[]>;
    groupList$: Observable<GroupModel[]>;

    dataSource$: Observable<DataSource>;

    lastInputMassChange: string;

    constructor(
        private store: Store<fromApp.AppState>,
        public dialogRef: MatDialogRef<ProjectComponent>) {

        this.mealOption$ = this.store.select(selectMealOptionState);
    }

    ngOnInit() {
        this.productFilter.valueChanges.subscribe((input: string) => this.store.dispatch(projectsActions.searchProductByName({ input: input })));
        this.groupFilter.valueChanges.subscribe((input: string) => this.store.dispatch(projectsActions.searchGroupByName({ input: input })));

        this.productList$ = this.store.select('project').pipe(takeUntil(this.onDestroy$), map(s => s.productList));
        this.groupList$ = this.store.select('project').pipe(takeUntil(this.onDestroy$), map(s => s.groupList));

        this.store.select('project').pipe(takeUntil(this.onDestroy$)).subscribe(s => {
            if (this.lastInputMassChange) {
                setTimeout(() => {
                    const input = document.getElementById(this.lastInputMassChange);
                    input.focus();
                });
            }
        });

        this.store.dispatch(projectsActions.loadData());
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    onMassChange(event: { product, newMass }) {
        const { product, newMass } = event;
        this.lastInputMassChange = `input-mass-${product.id}`;
        this.store.dispatch(projectsActions.changeMass({ mass: newMass, product: product }));
    }

    addItem(item: ProductModel | GroupModel) {
        if (item instanceof GroupModel) {
            this.store.dispatch(projectsActions.addGroup({group: item }))
        } else if (item instanceof ProductModel) {
            this.store.dispatch(projectsActions.addFood({food: item }))
        }
    }

    removeFood(prod: ProductModel) {
        this.store.dispatch(projectsActions.removeFood({food: prod }));
    }

    removeGroup(group: GroupModel) {
        this.store.dispatch(projectsActions.removeGroup({group: group }));
    }

    onTabChange(event: MatTabChangeEvent) {
        this.tabSelected = event.index;
    }

    saveMealOption() {
        this.store.dispatch(projectsActions.saveMealOption())
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    } */
}
