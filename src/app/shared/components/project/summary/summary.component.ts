import { Component, Input, OnInit } from '@angular/core';
import { MealModel } from '@core/models/meal.model';
import { ProductModel } from '@core/models/product.model';

@Component({
    selector: 'summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
    
    @Input() data: MealModel | ProductModel[];
    @Input() showProgressBar = false;
    @Input() simple = false;

    ngOnInit() {
    }

}