import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromProjectReducer from './project.reducer';

export const selectProjectState = createFeatureSelector<fromProjectReducer.State>('project');
/* 
export const selectMealOptionState = createSelector(
    selectProjectState,
  (state: fromProjectReducer.State) => state.mealOptionSelected
);
 */