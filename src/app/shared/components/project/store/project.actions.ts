import { GroupModel } from '@core/models/group.model';
import { ProductModel } from '@core/models/product.model';
import { createAction, props } from '@ngrx/store';

export const SELECT_MEAL_OPTION = '[Project] Select meal option';
export const LOAD_DATA = '[Project] Load data';
export const FILL_DATA = '[Project] Fill data';
export const SEARCH_PRODUCT = '[Project] Search product';
export const SEARCH_GROUP = '[Project] Search group';
export const CHANGE_MASS = '[Project] Change mass';
export const SAVE_MEAL_OPTION = '[Project] Save meal option';
export const ADD_FOOD = '[Project] Add food';
export const ADD_FOOD_SUCCESS = '[Project] Add food success';
export const REMOVE_FOOD = '[Project] Remove food';
export const REMOVE_FOOD_SUCCESS = '[Project] Remove food success';
export const ADD_GROUP = '[Project] Add group';
export const ADD_GROUP_SUCCESS = '[Project] Add group success';
export const REMOVE_GROUP = '[Project] Remove group';
export const REMOVE_GROUP_SUCCESS = '[Project] Remove group success';

/* export const selectMealOption = createAction(
    SELECT_MEAL_OPTION,
    props<{ indexMeal: number, mealOption: MealOptionModel, indexMealOption: number  }>()
) */

export const loadData = createAction(
    LOAD_DATA
)

export const fillData = createAction(
    FILL_DATA,
    props<{ products: ProductModel[], groups: GroupModel[] }>()
)

export const searchProductByName = createAction(
    SEARCH_PRODUCT,
    props<{ input: string }>()
)

export const searchGroupByName = createAction(
    SEARCH_GROUP,
    props<{ input: string }>()
)

export const changeMass = createAction(
    CHANGE_MASS,
    props<{ mass: number, product: ProductModel }>()
)

export const saveMealOption = createAction(
    SAVE_MEAL_OPTION
)

export const addFood = createAction(
    ADD_FOOD,
    props<{ food: ProductModel}>()
)

/* export const addFoodSuccess = createAction(
    ADD_FOOD_SUCCESS,
    props<{ productsLoaded: ProductModel[], mealOption: MealOptionModel }>()
) */

export const removeFood = createAction(
    REMOVE_FOOD,
    props<{ food: ProductModel}>()
)
/* 
export const removeFoodSuccess = createAction(
    REMOVE_FOOD_SUCCESS,
    props<{ productsLoaded: ProductModel[], mealOption: MealOptionModel }>()
)
 */
export const addGroup = createAction(
    ADD_GROUP,
    props<{ group: GroupModel}>()
)

/* export const addGroupSuccess = createAction(
    ADD_GROUP_SUCCESS,
    props<{ groupsLoaded: GroupModel[], mealOption: MealOptionModel }>()
) */

export const removeGroup = createAction(
    REMOVE_GROUP,
    props<{ group: GroupModel}>()
)

/* export const removeGroupSuccess = createAction(
    REMOVE_GROUP_SUCCESS,
    props<{ groupsLoaded: GroupModel[], mealOption: MealOptionModel }>()
) */