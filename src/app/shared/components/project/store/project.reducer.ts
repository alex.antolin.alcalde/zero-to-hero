import { GroupModel } from "@core/models/group.model";
import { ProductModel } from "@core/models/product.model";
import { createReducer, on } from "@ngrx/store";
import * as cloneDeep from 'lodash/cloneDeep';
import Utils, { filterItemsByName } from "src/app/utils/utils";
import * as projectActions from './project.actions';

export interface State {
    mealSelectedIndex: number;
    mealOptionIndex: number;
    productsLoaded: ProductModel[],
    productList: ProductModel[],
    groupsLoaded: GroupModel[],
    groupList: GroupModel[],
    inputSearch: string;
}

const initialState: State = {
    mealSelectedIndex: -1,
    mealOptionIndex: -1,
    productsLoaded: [],
    productList: [],
    groupsLoaded: [],
    groupList: [],
    inputSearch: ''
}

const _projectReducer = createReducer(initialState,
  /*   on(projectActions.selectMealOption, (state, { indexMeal, mealOption, indexMealOption  }) => {
        return {
            ...state,
            mealSelectedIndex: indexMeal,
            mealOptionSelected: mealOption,
            mealOptionIndex: indexMealOption

        }
    }),
    on(projectActions.fillData, (state, { products, groups }) => {
        return {
            ...state,
            productsLoaded: [...products],
            productList: [...products],
            groupsLoaded: [...groups],
            groupList: [...groups]
        }
    }),
    on(projectActions.searchProductByName, (state, { input }) => {
        const productsFiltered = filterItemsByName(state.productsLoaded, input) as ProductModel[];

        return {
            ...state,
            productList: productsFiltered,
            inputSearch: input
        }
    }),
    on(projectActions.searchGroupByName, (state, { input }) => {
        const groupsFiltered = filterItemsByName(state.groupsLoaded, input) as GroupModel[];
        return {
            ...state,
            groupList: groupsFiltered,
            inputSearch: input
        }
    }),
    on(projectActions.addFoodSuccess, (state, { productsLoaded, mealOption }) => {
        const productsFiltered = filterItemsByName(productsLoaded, state.inputSearch) as ProductModel[];

        return {
            ...state,
            mealOptionSelected: mealOption,
            productsLoaded: [...productsLoaded],
            productList: [...productsFiltered]
        }
    }),
    on(projectActions.removeFoodSuccess, (state, { productsLoaded, mealOption }) => {
        const productsFiltered = filterItemsByName(productsLoaded, state.inputSearch) as ProductModel[];

        return {
            ...state,
            mealOptionSelected: mealOption,
            productsLoaded: [...productsLoaded],
            productList: [...productsFiltered]
        }
    }),
    on(projectActions.addGroupSuccess, (state, { groupsLoaded, mealOption }) => {
        const groupsFiltered = filterItemsByName(groupsLoaded, state.inputSearch) as GroupModel[];

        return {
            ...state,
            mealOptionSelected: mealOption,
            groupsLoaded: [...groupsLoaded],
            groupList: [...groupsFiltered]
        }
    }),
    on(projectActions.removeGroupSuccess, (state, { groupsLoaded, mealOption }) => {
        const groupsFiltered = filterItemsByName(groupsLoaded, state.inputSearch) as GroupModel[];

        return {
            ...state,
            mealOptionSelected: mealOption,
            groupsLoaded: [...groupsLoaded],
            groupList: [...groupsFiltered]
        }
    }) */
)

export function projectReducer(state, action) {
    return _projectReducer(state, action)
}
