import { Injectable } from '@angular/core';
import { GroupModel } from '@core/models/group.model';
import { ProductModel } from '@core/models/product.model';
import { ApiGroupService } from '@core/services/api/api-group.service';
import { ApiProductService } from '@core/services/api/api-product.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { EMPTY, forkJoin } from 'rxjs';
import { catchError, exhaustMap, map, withLatestFrom } from 'rxjs/operators';
import { selectUserState } from 'src/app/auth/state/auth.selectors';
import * as fromApp from 'src/app/store/app.reducer';
import * as projectActions from './project.actions';

@Injectable()
export class ProjectEffects {

/* 
    loadData$ = createEffect(() => this.actions$.pipe(
        ofType(projectActions.loadData),
        withLatestFrom(
            this.store$.select('project'),
            this.store$.select(selectUserState)
        ),
        exhaustMap(([action, projectState, user]) => {
            const products$ = this.apiProduct.getProducts();
            const groups$ = this.apiGroup.getGroups();
            const mealOption = projectState.mealOptionSelected;
           
            return forkJoin([products$, groups$]).pipe(
                map(([products, groups]) => {
                    products = ProductModel.deserializeArray(products, ProductModel);
                    groups = GroupModel.deserializeArray(groups, GroupModel);

                    const productIds = mealOption.foods.map(p => p.id);
                   
                    const productList = products.filter(p => !productIds.includes(p.id));

                    const groupIds = mealOption.groups.map(p => p.id);
                    const groupList = groups.filter(p => !groupIds.includes(p.id));

                    return projectActions.fillData({ products: productList, groups: groupList });
                }),
                catchError(() => EMPTY)
            )

        })
    ));
 */
   /*  saveMeal$ = createEffect(() => this.actions$.pipe(
        ofType(projectActions.saveMealOption),
        withLatestFrom(
            this.store$.select('project'),
            this.store$.select('diet')
        ),
        exhaustMap(([action, projectState, dietState]) => {

            const index = projectState.mealSelectedIndex;
            const mealOption = projectState.mealOptionSelected;
            const mealOptionIndex = projectState.mealOptionIndex;

            const diet = cloneDeep(dietState.dietSelected) as DietModel;
            diet.meals[index][mealOptionIndex] = mealOption;

            return of(dietsActions.saveMeal({ diet: diet }));
        })
    )); */

  /*   addFood$ = createEffect(() => this.actions$.pipe(
        ofType(projectActions.addFood),
        withLatestFrom(
            this.store$.select('project')
        ),
        exhaustMap(([action, projectState]) => {
            // Remove product of list
            const productsLoaded = cloneDeep(projectState.productsLoaded);
            const index = productsLoaded.findIndex((prod: ProductModel) => prod.id === action.food.id);
            productsLoaded.splice(index, 1);


            // Push new food to meal
            const mealOption = cloneDeep(projectState.mealOptionSelected) as MealOptionModel;
            mealOption.foods = [...mealOption.foods, action.food];

            return of(projectActions.addFoodSuccess({ productsLoaded: productsLoaded, mealOption: mealOption }));
        })
    )); */
/* 
    removeFood$ = createEffect(() => this.actions$.pipe(
        ofType(projectActions.removeFood),
        withLatestFrom(
            this.store$.select('project')
        ),
        exhaustMap(([action, projectState]) => {
            // Push new food to meal
            const mealOption = cloneDeep(projectState.mealOptionSelected) as MealOptionModel;
            mealOption.foods = mealOption.foods.filter(f => f.id !== action.food.id);

            // Remove product of list
            const productsLoaded = cloneDeep(projectState.productsLoaded) as ProductModel[];
            productsLoaded.push(action.food);

            return of(projectActions.removeFoodSuccess({ productsLoaded: productsLoaded, mealOption: mealOption }));
        })
    ));
 */
/*     addGroup$ = createEffect(() => this.actions$.pipe(
        ofType(projectActions.addGroup),
        withLatestFrom(
            this.store$.select('project')
        ),
        exhaustMap(([action, projectState]) => {
            // Remove product of list
            const groupsLoaded = cloneDeep(projectState.groupsLoaded);
            const index = groupsLoaded.findIndex((group: GroupModel) => group.id === action.group.id);
            groupsLoaded.splice(index, 1);

            // Push new food to meal
            const mealOption = cloneDeep(projectState.mealOptionSelected) as MealOptionModel;
            mealOption.groups = [...mealOption.groups, action.group];

            return of(projectActions.addGroupSuccess({ groupsLoaded: groupsLoaded, mealOption: mealOption }));
        })
    ));
 */
    /* 
    removeGroup$ = createEffect(() => this.actions$.pipe(
        ofType(projectActions.removeGroup),
        withLatestFrom(
            this.store$.select('project')
        ),
        exhaustMap(([action, projectState]) => {
            // Push new food to meal
            const mealOption = cloneDeep(projectState.mealOptionSelected) as MealOptionModel;
            mealOption.groups = mealOption.groups.filter(f => f.id !== action.group.id);

            // Remove product of list
            const groupsLoaded = cloneDeep(projectState.groupsLoaded) as GroupModel[];
            groupsLoaded.push(action.group);

            return of(projectActions.removeGroupSuccess({ groupsLoaded: groupsLoaded, mealOption: mealOption }));
        })
    ));
 */




    constructor(
        private store$: Store<fromApp.AppState>,
        private actions$: Actions,
        private apiProduct: ApiProductService,
        private apiGroup: ApiGroupService) { }
}
