import { Injectable } from '@angular/core';
import { ProductModel } from '@core/models/product.model';
import { Summary } from '@core/models/summary.model';
import { Observable } from 'rxjs';
import { DataSource, ProjectState } from './project.state.service';

@Injectable()
  export class ProjectFacade {
  
  constructor(
    private projectState: ProjectState) { }
  
  getDatasource$(): Observable<DataSource> {
    return this.projectState.getDatasource$();
  }

  getSummary$(): Observable<Summary> {
    return this.projectState.getSummary$();
  }

  loadProject(meal): void {
    this.projectState.setMeal(meal)
  }

  loadSummary(summary): void {
    this.projectState.setSummary(summary);
  }

  changeMass(prod: ProductModel, newMass: number): void {
    this.projectState.changeMass(prod, newMass);
    this.projectState.calculateSummary();
  }

}