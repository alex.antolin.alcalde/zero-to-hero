import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormControl } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog } from "@angular/material/dialog";
import { MatSelectChange } from "@angular/material/select";
import { MealModel } from "@core/models/meal.model";
import { selectDietListState, selectDietSelectedState } from "@features/diet/store/diet.selectors";
import { DietModel } from '@models/diet.model';
import { Store } from "@ngrx/store";
import { ModifyDietComponent } from "@shared/components/modify-diet/modify-diet.component";
import { ProjectComponent } from "@shared/components/project/project.component";
import { ConfirmDeleteTemplateComponent } from "@shared/components/remove-template/confirm-delete-template.component";
// the `default as` syntax.
import * as moment from 'moment';
import { Observable } from 'rxjs';
import * as fromApp from 'src/app/store/app.reducer';
import { DietService } from "../../../../features/diet/diet.service";
import { AddMealComponent } from '../../add-meal/add-meal.component';
import { CreateDietComponent } from '../../create-diet/create-diet.component';
// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-diet-menu',
    templateUrl: 'diet-menu.component.html',
    styleUrls: ['./diet-menu.component.scss'],
    providers: [
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    ],
    encapsulation: ViewEncapsulation.None
})
export class DietMenuComponent implements OnInit {


    date = new FormControl(this.currentDate);
    maxDate = moment().toDate();

    dietList$: Observable<DietModel[]> = this.store.select(selectDietListState);
    dietSelected: DietModel;
    view$: Observable<'current' | 'template'>;

    get currentDate() { return moment(); }

    constructor(
        private store: Store<fromApp.AppState>,
        private dietService: DietService,
        private dialog: MatDialog) { }

    ngOnInit() {
        this.store.select(selectDietSelectedState).subscribe(diet => {
           if (this.dietSelected?.id !== diet?.id) {
               this.dietSelected = diet;
           }  
        })
    }

    goToProject() {
        this.dialog.open(ProjectComponent, {
            minHeight: '640px',
            maxHeight: '90%',
            minWidth: '1090px',
            maxWidth: '90%'
        });
    }

    onDietChanged(event: MatSelectChange) {
        const { value } = event;
        this.dietService.selectDiet(value)
    }


    createDiet() {
        this.dialog.open(CreateDietComponent, {
            width: '350px'
        });
    }

    saveChanges() {
        this.dialog.open(ModifyDietComponent, {
            width: '350px',
            height: '220px',
            disableClose: true,
            backdropClass: 'action-dialog-backdrop',
            autoFocus: false,
        });
    }

    deleteDiet() {
        this.dialog.open(ConfirmDeleteTemplateComponent, {
            width: '405px',
            height: '220px',
            disableClose: true,
            backdropClass: 'action-dialog-backdrop',
            autoFocus: false,
        });
    }

    public addMeal() {
        this.dialog.open(AddMealComponent, {
            width: '350px'
        });
    }
}
