import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GroupModel } from '@core/models/group.model';
import { ProductModel } from '@models/product.model';
import { Store } from '@ngrx/store';
import { DeleteProductComponent } from '@shared/components/confirm/delete-product.component';
import { CreateGroupComponent } from '@shared/components/create-group/create-group.component';
import { CreateProductComponent } from '@shared/components/create-product/create-product.component';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as fromApp from '../../../../store/app.reducer';

@Component({
    selector: 'app-product-menu',
    templateUrl: 'product-menu.component.html',
    styleUrls: ['./product-menu.component.scss']
})
export class ProductMenuComponent implements OnInit {

    productsSelected$: Observable<ProductModel[]>;

    constructor(
        private store: Store<fromApp.AppState>,
        public dialog: MatDialog) { }

    ngOnInit() {
        this.productsSelected$ = this.store.select('productList').pipe(map(state => state.productsSelected));
    }

    createProduct() {
        this.dialog.open(CreateProductComponent, {
            width: '780px',
            disableClose: true
        });
    }

    createGroup(): void {
        this.dialog.open(CreateGroupComponent, {
            width: '750px',
            disableClose: true,
            backdropClass: 'add-price-backdrop'
        });
    }

    deleteProducts(): void {

        this.dialog.open(DeleteProductComponent, {
            width: '420px'
        });
    }
}
