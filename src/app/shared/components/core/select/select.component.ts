import { AfterViewInit, Component, ContentChildren, EventEmitter, Input, Output, QueryList } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { MatOption } from "@angular/material/core";


@Component({
  selector: 'z2h-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SelectComponent
    }
  ]
})
export class SelectComponent<T> implements ControlValueAccessor, AfterViewInit {
  @ContentChildren(MatOption) queryOptions: QueryList<MatOption>;
  options: any[];
  yet: boolean;

  @Input() label: string;
  @Input() prefixIcon: string;
  @Input() type: string = 'text';
  @Output() onSelectionChange: EventEmitter<any> = new EventEmitter<any>();

  value: T;

  onChange = (value) => { };

  onTouched = () => { };

  touched = false;

  disabled = false;

  ngAfterViewInit() {
    this.options = this.queryOptions.map(x => {
      return { value: x.value, viewValue: x.viewValue };
    });
    console.log(this.options)
    setTimeout(() => {
      this.yet = true;
    });
  }

  writeValue(value: T) {
    this.value = value;
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

}