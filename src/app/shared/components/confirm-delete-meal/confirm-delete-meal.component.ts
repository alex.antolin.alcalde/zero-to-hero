import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DietModel } from "@core/models/diet.model";
import { Store } from "@ngrx/store";
import * as fromApp from 'src/app/store/app.reducer';
import * as dietsActions from '@features/diet/store/diet.actions';

@Component({
    selector: 'z2h-confirm-delete-meal',
    templateUrl: 'confirm-delete-meal.component.html',
})
export class ConfirmDeleteMealComponent {

    template: DietModel;

    constructor(
        private store: Store<fromApp.AppState>,
        public dialogRef: MatDialogRef<ConfirmDeleteMealComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { indexMeal: number }
        ) {
    }

    close() {
        this.dialogRef.close();
    }

    confirm() {
        this.store.dispatch(dietsActions.deleteMeal({ indexMeal: this.data.indexMeal }))
        this.dialogRef.close(true);
    }
}
