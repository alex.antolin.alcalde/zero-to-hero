import { Component, Inject, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import * as fromApp from 'src/app/store/app.reducer';
import * as dietsActions from '@features/diet/store/diet.actions';

@Component({
    templateUrl: './edit-diet.component.html',
    styleUrls: ['./edit-diet.component.scss']
})
export class EditDietDialogComponent implements OnInit {
    cloneDayForm: FormGroup
    days: string[] = [
        'Domingo',
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado'
    ]

    get dayToClone() {
        return this.cloneDayForm.controls["dayToClone"] as FormArray;
    }


    get daysToBeOverride() {
        return this.cloneDayForm.controls["daysToBeOverride"] as FormArray;
    }

    constructor(
        private store: Store<fromApp.AppState>,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<EditDietDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    ngOnInit(): void {
        this.buildForm();
    }

    close() {
        this.dialogRef.close();
    }

    cloneDays() {
        if(this.cloneDayForm.valid) {
            this.store.dispatch(dietsActions.cloneDayOfDiet({
                dayToBeClone: this.dayToClone.value,
                daysToBeOverride: this.daysToBeOverride.value.map(d => d['day'])
    
            }))
        }
       
    }

    private buildForm(): void {
        this.cloneDayForm = this.fb.group({
            dayToClone: new FormControl(0, [Validators.required]),
            daysToBeOverride: this.fb.array([])
        })

        this.days.forEach(d => {
            this.daysToBeOverride.push(this.fb.group({
                day: new FormControl(false)
            }))
        })
        
    }
}