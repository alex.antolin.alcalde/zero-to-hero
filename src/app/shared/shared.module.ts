import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ShowdownModule } from 'ngx-showdown';
import { ProfileComponent } from '../features/profile/profile.component';
import { AddMealComponent } from './components/add-meal/add-meal.component';
import { AddPriceComponent } from './components/add-price/add-price.component';
import { GetDecimalPipe } from './components/add-price/get-decimal.pipe';
import { ChangelogComponent } from './components/changelog/changelog.component';
import { CommentsComponent } from './components/comments/comments.component';
import { ConfirmDeleteMealComponent } from './components/confirm-delete-meal/confirm-delete-meal.component';
import { DeleteProductComponent } from './components/confirm/delete-product.component';
import { CreateDietComponent } from './components/create-diet/create-diet.component';
import { CreateGroupComponent } from './components/create-group/create-group.component';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { DayOfWeekSelector } from './components/day-of-week-selector/day-of-week-selector.component';
import { EditDietDialogComponent } from './components/edit-diet/edit-diet.component';
import { Error404Component } from './components/error/404.component';
import { GroupListComponent } from './components/group-list/group-list.component';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { DietMenuComponent } from './components/menu/diet-menu/diet-menu.component';
import { ProductMenuComponent } from './components/menu/product-menu/product-menu.component';
import { ModifyDietComponent } from './components/modify-diet/modify-diet.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProjectComponent } from './components/project/project.component';
import { SummaryComponent } from './components/project/summary/summary.component';
import { RegisterWeightComponent } from './components/register-weight/register-weight.component';
import { ConfirmDeleteTemplateComponent } from './components/remove-template/confirm-delete-template.component';
import { ReportDietComponent } from './components/report-diet/report-diet.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { UploadComponent } from './components/upload/upload.component';
import { WeightHistoricComponent } from './components/weight-historic/weight-historic.component';
import { DragDropDirective } from './directives/drag-drop.directive';
import { MaterialModule } from './material.module';
import { EnumToArrayPipe } from './pipes/enum.pipe';
import { SafeStylePipe } from './pipes/safe-style.pipe';
import { SummaryPipe } from './pipes/summary.pipe';
import { InputModule } from './components/core/input/input.module';
import { SelectModule } from './components/core/select/select.module';


@NgModule({
    declarations: [
        AddPriceComponent,
        ModifyDietComponent,
        GetDecimalPipe,
        SummaryPipe,
        EnumToArrayPipe,
        LoadingIndicatorComponent,
        ProductMenuComponent,
        DietMenuComponent,
        Error404Component,
        CreateDietComponent,
        ConfirmDeleteMealComponent,
        ConfirmDeleteTemplateComponent,
        AddMealComponent,
        RegisterWeightComponent,
        ProfileComponent,
        CreateProductComponent,
        CreateGroupComponent,
        SnackBarComponent,
        DeleteProductComponent,
        ProjectComponent,
        ProductListComponent,
        GroupListComponent,
        SummaryComponent,
        ChangelogComponent,
        UploadComponent,
        DragDropDirective,
        SafeStylePipe,
        WeightHistoricComponent,
        ReportDietComponent,
        DayOfWeekSelector,
        CommentsComponent,
        EditDietDialogComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        ShowdownModule,
        TranslateModule,
        InputModule,
        SelectModule
    ],
    exports: [
        AddPriceComponent,
        ModifyDietComponent,
        GetDecimalPipe,
        SummaryPipe,
        EnumToArrayPipe,
        LoadingIndicatorComponent,
        MaterialModule,
        ProductMenuComponent,
        DietMenuComponent,
        Error404Component,
        CreateDietComponent,
        ConfirmDeleteMealComponent,
        ConfirmDeleteTemplateComponent,
        AddMealComponent,
        RegisterWeightComponent,
        CreateProductComponent,
        CreateGroupComponent,
        DeleteProductComponent,
        SnackBarComponent,
        ProjectComponent,
        ProductListComponent,
        GroupListComponent,
        SummaryComponent,
        ProfileComponent,
        ChangelogComponent,
        TranslateModule,
        DragDropDirective,
        SafeStylePipe,
        WeightHistoricComponent,
        ReportDietComponent,
        DayOfWeekSelector,
        CommentsComponent,
        EditDietDialogComponent,
        InputModule,
        SelectModule
    ],
    entryComponents: [
        AddPriceComponent,
        ModifyDietComponent,
        CreateDietComponent,
        ConfirmDeleteMealComponent,
        ConfirmDeleteTemplateComponent,
        CreateProductComponent,
        CreateGroupComponent,
        DeleteProductComponent,
        SnackBarComponent,
        ProjectComponent,
        AddMealComponent,
        RegisterWeightComponent,
        ProfileComponent,
        ChangelogComponent,
        ReportDietComponent,
        EditDietDialogComponent
    ]
  })
export class SharedModule {

}
