import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './features/login/login.component';
import { AuthGuard } from './auth/auth.guard';

import { Error404Component } from '@shared/components/error/404.component';
import { AppLayoutComponent } from '@shared/components/layout/layout.component';

const routes: Routes = [
  { path: '',
    redirectTo: '/diets',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AppLayoutComponent,
    data: {
      title: 'Zero to HERO'
    },
    children: [
      {
        path: 'diets',
        loadChildren: () => import('./features/diet/diet.module').then(m => m.DietModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'products',
        loadChildren: () => import('./features/product/product.module').then(m => m.ProductModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'progress',
        loadChildren: () => import('./features/progress/progress.module').then(m => m.ProgressModule),
        canActivate: [AuthGuard]
      }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: Error404Component }
];


@NgModule({
  imports: [RouterModule.forRoot([...routes], {
    useHash: true
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
