import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
// Google firestore
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CoreModule } from '@core/core.module';
import { ApiInterceptor } from '@core/httpinterceptors/api.interceptor';
import { customNotifierOptions } from '@core/notifierConfig';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { environment } from '@env/environment';
import { DietEffects } from '@features/diet/store/diet.effects';
import { FooterComponent } from '@features/footer/footer.component';
import { UserProfileComponent } from '@features/footer/user-profile/user-profile.component';
import { HeaderComponent } from '@features/header/header.component';
import { LoginComponent } from '@features/login/login.component';
import { ProductEffects } from '@features/product/state/product-list.effects';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppLayoutComponent } from '@shared/components/layout/layout.component';
import { ProjectEffects } from '@shared/components/project/store/project.effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Components
import { ChangelogHttpService } from './core/services/changelog.http.service';
import * as fromApp from './store/app.reducer';
import { AuthEffects } from './auth/state/auth.effects';
import { AngularFireModule } from '@angular/fire/compat';
import { browserSessionPersistence, getAuth, setPersistence, signInWithEmailAndPassword } from 'firebase/auth';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function app_Init(changelogHttpService: ChangelogHttpService) {
  return () => changelogHttpService.initializeApp()}


@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent,
    HeaderComponent,
    FooterComponent,
    UserProfileComponent,
    LoginComponent
  ],
  imports: [
    CoreModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([AuthEffects, ProductEffects, DietEffects, ProjectEffects]),
    AngularFireModule.initializeApp(environment.firebase),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    AuthGuard,
    { provide: APP_INITIALIZER, useFactory: app_Init, deps: [ChangelogHttpService], multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    }
/*     {
      provide: UrlSerializer,
      useClass: StandardUrlSerializer
    } */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
