// Utilizar funcionalidades del Ecmascript 6
'use strict'

// Cargamos los módulos de express y body-parser
var express = require('express');
var bodyParser = require('body-parser');

// Llamamos a express para poder crear el servidor
var app = express();


var express = require('express');
var cors = require('cors');

// Importamos las rutas
var product_routes = require('./routes/product'); 
var diet_routes= require('./routes/diet'); 

//cargar middlewares
//un metodo que se ejecuta antes que llegue a un controlador
//Configuramos bodyParser para que convierta el body de nuestras peticiones a JSON
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(cors())

app.use(express.static(__dirname + '/dist'));
// Cargamos las rutas
app.use('/api', product_routes);
app.use('/api', diet_routes);

// exportamos este módulo para poder usar la variable app fuera de este archivo
module.exports = app;