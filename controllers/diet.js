'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Cargamos los modelos para usarlos posteriormente
var Diet = require('../models/diet');

// Get diet by date
exports.getDiet = (req, res) => {
	let date = req.query.date;
    let userId = req.query.userId;
    Diet.findOne({userId: userId, date: date}, (err, diet) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});
        res.send(diet); 
    });
}

exports.sendDiet = (req, res) => {
    let diet = req.body;
    var query = {date: diet.date};
    delete diet._id;
    var update = diet;
    Diet.findOneAndUpdate(query, update, {upsert: true}, function (err, diet) {
        if(err) return res.status(500).send({message: 'Error en la petición'});
        res.status(201).send(diet)
    });
}