'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Cargamos los modelos para usarlos posteriormente
var Product = require('../models/product');

// Get all Products
exports.getProduct = (req, res) => {
    Product.find({}, (err, products) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});
        res.send(products); 
    });
}

// Get Product by id
exports.getProductById = (req, res) => {
    var productId = req.params.id;
    //buscar un documento por un  id
    Product.findById(productId, (err, product) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});
        if(!product) return res.status(404).send({message: 'La comida no existe'});
        res.send(product);
    });
}

exports.sendProduct = (req, res) => {
    let prod = req.body;
    var query = {_id: prod._id};
    if (!query._id) {
        query._id = new mongoose.mongo.ObjectID();
    }
    delete prod._id;
    var update = prod;

    Product.findOneAndUpdate(query, update, {upsert: true}, function (err, product) {
        if(err) return res.status(500).send({message: 'Error en la petición'});
        res.status(201).send(product)
    });
}

exports.deleteProducts = (req, res) => {
    let ids = req.body.ids;
    const query = {'_id':{'$in': ids}};
    Product.remove(query, (err, result) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});
        res.status(200).send(result);
    });
}